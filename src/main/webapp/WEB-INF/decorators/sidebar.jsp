
<nav class="navbar" style="height: 100%; position: fixed;"
	role="navigation">
	<aside class="main-sidebar">

		<section class="sidebar">
			<div id = "menulist">
				<c:forEach var="menuModel" items="${menuModelList}" varStatus="num">
					<a style="color:black;">${menuModel.codeMenu}></a>
					<a href="${contextName}/${menuModel.controllerName}.html" class="menu-item">
					> ${menuModel.nameMenu}</a>
					<br/>
				
				</c:forEach>
			
			</div>
		</section>
		<!-- /.sidebar -->
	</aside>
</nav>