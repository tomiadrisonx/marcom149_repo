<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-role">
	<input type="hidden" id="process" name="process" value="delete"/>
	<div class="form-horizontal">
		<h4>Delete Role - ${roleModel.nameRole} (${roleModel.codeRole})</h4>
		<input type="hidden" id="idRole" name="idRole" value="${roleModel.idRole}"/>
		<div class="form-group">
			<label class="control-label col-md-3">*Role Code</label>
			<div class="col-md-6">
				<input type="hidden" id="codeRole" name="codeRole" value="${roleModel.codeRole}"/>
				<input type="text" class="form-input " id="codeRoleDisplay"
					name="codeRoleDisplay" oninput="setCustomValidity('')" disabled="disabled" value="${roleModel.codeRole}"/>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Role Name</label>
			<div class="col-md-6">
				<input type="hidden" id="nameRole" name="nameRole" value="${roleModel.nameRole}"/>
				<input type="text" class="form-input " id="nameRoleDisplay"
					name="nameRoleDisplay" oninput="setCustomValidity('')" disabled="disabled" value="${roleModel.nameRole}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Description</label>
			<input type="hidden" id="descriptionRole" name="descriptionRole" value="${roleModel.descriptionRole}"/>
			<div class="col-md-6">
				<textarea rows="5" cols="50" class="form-input" id="descriptionRoleDisplay"
					name="descriptionRoleDisplay" disabled="disabled"></textarea>
			</div>
		</div>

		<div class="modal-footer">
			Delete Data?
			<button type="submit" class="btn btn-danger" id="btn-save">Delete</button>
		</div>

	</div>
</form>

<script type="text/javascript">
	function validasiInput() {
		var nameRole = document.getElementById("nameRole");

		if (nameRole.value == '') {
			nameRole.setCustomValidity('Role Name Empty');
		} else {

		}

	}
</script>