<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach var="roleModel" items="${roleModelList }">

	<tr>
		<td>${roleModel.codeRole}</td>
		<td>${roleModel.nameRole}</td>
		<td>${roleModel.descriptionRole}</td>
		<td>${roleModel.createdDateRole}</td>
		<td>${roleModel.createdByRole}</td>			
		<td>
			<button type="button" id="btn-edit" class="btn btn-success btn-xs btn-edit" value="${roleModel.idRole}">Edit</button>
			<button type="button" id="btn-delete" class="btn btn-danger btn-xs btn-delete" value="${roleModel.idRole}">Delete</button>
			
		</td>
	</tr>
	

</c:forEach>