<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-division">
	<input type="hidden" id="process" name="process" value="delete" />
	<div class="form-horizontal">
		<h1>Add Division</h1>
		<input type="hidden" id="idDivision" name="idDivision"
			value="${divisionModel.idDivision}" />

		<div class="form-group">
			<label class="control-label col-md-3">Division Code</label>
			<div class="col-md-6">
				<input type="hidden" id="codeDivision" name="codeDivision"
					value="${divisionModel.codeDivision}"> <input type="text"
					class="form-input" id="codeDivisionDisplay"
					name="codeDivisionDisplay" oninput="setCustomValidity('')"
					disabled="disabled" value="${divisionModel.codeDivision}" />
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Division Name</label>
			<div class="col-md-6">
				<input type="hidden" id="nameDivision" name="nameDivision"
					value="${divisionModel.nameDivision}" /> <input type="text"
					class="form-input" id="nameDivisionDisplay"
					name="nameDivisionDisplay" oninput="setCustomValidity('')"
					value="${divisionModel.nameDivision}" disabled="disabled" />
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Description</label>
			<div class="col-md-6">
				<input type="hidden" id="descriptionDivision"
					name="descriptionDivision"
					value="${divisionModel.descriptionDivision}" />
				<textarea rows="5" cols="50" class="form-input"
					id="descriptionDivisionDisplay" name="descriptionDivisionDisplay"
					oninput="setCustomValidity('')" disabled="disabled">${divisionModel.descriptionDivision}</textarea>
			</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-danger" id="btn-save"
				onclick="validasiInput();">Save</button>
		</div>
	</div>

</form>

<script>
	function validasiInput() {
		var nameDivision = document.getElementById("nameDivision");
		var descriptionDivision = document
				.getElementById("descriptionDivision");

		if (nameDivision.value == "") {
			nameDivision.setCustomValidity("Nama Supplier Tidak Boleh Kosong");
		} else if (descriptionDivision.value == "") {
			descriptionDivision
					.setCustomValidity("Tipe Supplier Tidak Boleh Kosong");
		} else {

		}
	}
</script>