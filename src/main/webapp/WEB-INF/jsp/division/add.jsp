<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-division">
	<input type="hidden" id="process" name="process" value="create" />
	<div class="form-horizontal">
		<h1>Add Division</h1>

		<div class="form-group">
			<label class="control-label col-md-3">Division Code</label>
			<div class="col-md-6">
				<input type="hidden" id="codeDivision" name="codeDivision"
					value="${codeDivisionGenerator}"> <input type="text"
					class="form-input" id="codeDivisionDisplay"
					name="codeDivisionDisplay" oninput="setCustomValidity('')"
					disabled="disabled" value="${codeDivisionGenerator}" />
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Division Name</label>
			<div class="col-md-6">
				<input type="text" class="form-input" id="nameDivision"
					name="nameDivision" oninput="setCustomValidity('')" />
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Description</label>
			<div class="col-md-6">
				<textarea rows="5" cols="50" class="form-input"
					id="descriptionDivision" name="descriptionDivision"
					oninput="setCustomValidity('')"></textarea>
			</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-success" id="btn-save"
				onclick="validasiInput();">Save</button>
		</div>
	</div>

</form>

<script>
function validasiInput() {
	var nameDivision = document.getElementById("nameDivision");

	if (nameDivision.value == "") {
		nameDivision.setCustomValidity("Division Name Could Not Be Empty");
	}else {

	}
}
</script>