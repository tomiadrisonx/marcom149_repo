<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach var="divisionModel" items="${divisionModelList}">

	<tr>
		<td>${divisionModel.codeDivision}</td>
		<td>${divisionModel.nameDivision}</td>
		<td>${divisionModel.descriptionDivision}</td>
		<td>
			<button type="button" id="btn-edit"
				class="btn btn-success btn-xs btn-edit"
				value="${divisionModel.idDivision}">Edit</button>
			<button type="button" id="btn-delete"
				class="btn btn-danger btn-xs btn-delete"
				value="${divisionModel.idDivision}">Delete</button>
		</td>
	</tr>

</c:forEach>