<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach var="companyModel" items="${companyModelList}">

	<tr>

		<td>${companyModel.idCompany}</td>
		<td>${companyModel.codeCompany}</td>
		<td>${companyModel.nameCompany}</td>
		<td><button type="button" id="btn-edit" class="btn btn-success btn-xs btn-edit" value="${companyModel.idCompany}">Edit</button>
		<button type="button" id="btn-delete" class="btn btn-danger btn-xs btn-delete" value="${companyModel.idCompany}">Delete</button>
		<button type="button" id="btn-detail" class="btn btn-info btn-xs btn-detail" value="${companyModel.idCompany}">Detail</button></td>

	</tr>

</c:forEach>