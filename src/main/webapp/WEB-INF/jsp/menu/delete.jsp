<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-menu">
	<input type="hidden" id="process" name="process" value="delete"/>
	<input type="hidden" id="idMenu" name="idMenu" value="${menuModel.idMenu}"/>
	<input type="hidden" id="codeMenu" name="codeMenu" value="${menuModel.codeMenu}"/>
	<input type="hidden" id="nameMenu" name="nameMenu" value="${menuModel.nameMenu}"/>
	<input type="hidden" id="controllerName" name="controllerName" value="${menuModel.controllerName}"/>
			<div class="modal-footer">
		Data ${menuModel.codeMenu} dengan nama ${menuModel.nameMenu} akan dihapus ?<br/>
			<button type="submit" class="btn btn-success" id="btn-save">Ya, hapus</button>
		</div>

</form>
