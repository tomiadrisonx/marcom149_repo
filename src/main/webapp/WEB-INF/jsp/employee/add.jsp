
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-employee">

	
<input type="hidden" id="process" name="process" value ="create"/>
		<div class=form-horizontal>
			<h1>Add Employee</h1>
			
			<div class="form-group">
			<label class="control-label col-md-3">Employee Code</label>
			<div class="col-md-6">
			<input type="hidden" id="codeEmployee" name="codeEmployee" value="${kodeEmployeeGenerator}"/>
				<input type="text" class="form-input" id="codeCompanyDisplay"
					name="codeCompanyDisplay" oninput="setCustomValidity('')" disabled="disabled" value="${kodeEmployeeGenerator}"/>
			</div>

		</div>

		<div class="form-group">
			<label class="control-label col-md-3">First Name</label>
			<div class="col-md-6">
				<input type="text" class="form-input" id="firstnameEmployee"
					name="firstnameEmployee" oninput="setCustomValidity('')" size="30" />
			</div>

		</div>
		


			<div class="form-group">
				<label class="control-label col-md-3">Last Name</label>
				<div class="col-md-6">
					<input type="text" class="form-input" id="lastnameEmployee"
						name="lastnameEmployee" oninput="setCustomValidity('')" size="30" />
				</div>
			</div>
			
			<div class="form-group">
			<label class="control-label col-md-3">Company</label>
			<div class="col-md-6">
				<select class="form-input" id="idCompany"
					name="idCompany" oninput="setCustomValidity('')">
					<c:forEach var="companyModel" items="${companyModelList}">
					<option value="${companyModel.idCompany}">${companyModel.nameCompany}</option>
					</c:forEach>
					</select>
			</div>
		</div>

			<div class="form-group">
				<label class="control-label col-md-3">Email</label>
				<div class="col-md-6">
					<input type="text" class="form-input" id="emailEmployee"
						name="emailEmployee" size="30" oninput="setCustomValidity('')" placeholder="marcom@marcom149.com" />
				</div>

			</div>


			<div class="modal-footer">
				<button type="submit" class="btn btn-success" id="btn-save" onclick="validasiInput();">Save</button>

			</div>
		
	</div>


</form>

<script type="text/javascript">

	function validasiAngka(evt) {
		var charAngka = (evt.which) ? evt.which : event.keyCode
		if ((charAngka > 31) && ((charAngka < 48) || (charAngka > 57))) {
			return false;
		} else {
			return true;
		}
	}

	function validasiInput() {
		var codeEmployee = document.getElementById("codeEmployee");
		var firstnameEmployee = document.getElementById("firstnameEmployee");
		
		if (codeEmployee.value == "") {
			codeEmployee.setCustomValidity("Employee Code Is Empty");
		}else if (firstnameEmployee.value == "") {
			firstnameEmployee.setCustomValidity("First Name Is Empty");
		}else {
			
		}
	}

</script>