<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach var="userModel" items="${userModelList}">

	<tr>		
		<td>${userModel.employeeModel.firstnameEmployee} ${userModel.employeeModel.lastnameEmployee}</td>
		<td>${userModel.roleModel.nameRole}</td>
		<td>${userModel.employeeModel.companyModel.nameCompany}</td>
		<td>${userModel.username}</td>
		<td>${userModel.createdDateUser}</td>
		<td>${userModel.createdByUser}</td>
		<td>
			<button type="button" id="btn-edit" class="btn btn-success btn-xs btn-edit" value="${userModel.idUser}">Edit</button>
			<button type="button" id="btn-delete" class="btn btn-danger btn-xs btn-delete" value="${userModel.idUser}">Delete</button>
		</td>
	</tr>
	

</c:forEach>