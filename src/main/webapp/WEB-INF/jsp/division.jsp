<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="panel"
	style="background: white; margin-top: 40px; min-height: 620px">
	<div class="box box-info">
		<div class="box-header">
			<h3 class="box-title">Division</h3>
			<div class="box-tools">
				<button type="button" id="btn-add"
					class="btn btn-primary pull-right">
					<i class="fa fa-plus"></i>Add Division
				</button>
			</div>
		</div>
		<div class="box-body">
			<table class="table" id="tbl_division">
				<thead>
					<tr>
						<th>Division Code</th>
						<th>Division Name</th>
						<th>Description</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="list-data-division">

				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- Modal Input untuk Pop Up -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Division</h4>
			</div>
			<div class="modal-body">
				<!-- isinya adalah add.jsp -->
			</div>
		</div>
	</div>
</div>
<!-- Akhir Modal Input untuk Pop Up -->

<script>

	/* ajax untuk list */
	$(function(){
		$('#tbl_division').DataTable({
			searching:true
		});
	});

	loadData();
	
	function loadData() {
		$.ajax({
			url:'division/list.html',
			type:'get',
			dataType:'html',
			success: function(data){
				$('#list-data-division').html(data);
			}
		});
	}
	/* akhir ajax untuk list */
	
	/* document ready function berfungsi untuk ajax ketika suatu button diklik */
	$(document).ready(function (){
		
		$("#modal-input").on('submit', '#form-division', function() {
			$.ajax({
				url:'division/save.json', /* json untuk kirim data ke controller */
				type: 'get',
				dataType: 'json',
				data : $(this).serialize(),
				success: function(data) {
					
					if (data.process == 'create') {
						alert('Data saved! New Division has been added with code '+data.divisionModel.codeDivision);
						/* jika button submit di klik maka pop up akan hide */
						$("#modal-input").modal('hide');
						loadData(); /* ini untuk refresh list nya */
					}else if (data.process == 'update') {
						alert('Data updated!');
						/* jika button submit di klik maka pop up akan hide */
						$("#modal-input").modal('hide');
						loadData();
					}else if (data.process == 'delete') {
						alert('Data deleted! Data division with code '+data.divisionModel.codeDivision+'has been deleted!');
						/* jika button submit di klik maka pop up akan hide */
						$("#modal-input").modal('hide');
						loadData();
					}else {
						
					}
					
					
					
				}
			});
			return false;
			
		});
		
		/* btn-add onclick fungsinya untuk ajax ketika button add diklik */
		$('#btn-add').on('click', function (){
			/* ajax adalah suatu fungsi atau method di jsp/front end */
			$.ajax({
				url:'division/add.html',
				type: 'get',
				dataType: 'html',
				success: function(data) {
					/* jika button add di klik maka akan muncul pop up / show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
				
			});
			/* akhir ajax adalah suatu fungsi atau method di jsp/front end */
		});
		/* akhir btn-add onclick fungsinya untuk ajax ketika button add diklik */
		
		/* btn-add onclick fungsinya untuk ajax ketika button add diklik */
		$('#list-data-division').on('click', '#btn-edit', function (){
			var vId = $(this).val();
			$.ajax({
				url:'division/edit.html',
				type: 'get',
				dataType: 'html',
				data:{idDivision:vId},
				success: function(data) {
					/* jika button add di klik maka akan muncul pop up / show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
				
			});
			/* akhir ajax adalah suatu fungsi atau method di jsp/front end */
		});
		/* akhir btn-add onclick fungsinya untuk ajax ketika button add diklik */
		
		$('#list-data-division').on('click', '#btn-delete', function (){
			var vId = $(this).val();
			$.ajax({
				url:'division/delete.html',
				type: 'get',
				dataType: 'html',
				data:{idDivision:vId},
				success: function(data) {
					/* jika button add di klik maka akan muncul pop up / show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
				
			});
			/* akhir ajax adalah suatu fungsi atau method di jsp/front end */
		});
		
		$('#list-data-division').on('click', '#btn-detail', function (){
			var vId = $(this).val();
			$.ajax({
				url:'division/detail.html',
				type: 'get',
				dataType: 'html',
				data:{idDivision:vId},
				success: function(data) {
					/* jika button add di klik maka akan muncul pop up / show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
				
			});
			/* akhir ajax adalah suatu fungsi atau method di jsp/front end */
		});

	});
	/* akhir document ready function berfungsi untuk ajax ketika suatu button diklik */

</script>