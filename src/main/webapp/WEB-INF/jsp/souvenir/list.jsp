<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach var="souvenirModel" items="${souvenirModelList}">

	<tr>
		<td>${souvenirModel.codeSouvenir}</td>
		<td>${souvenirModel.nameSouvenir}</td>
		<td>${souvenirModel.unit.nameUnit}</td>
		<td>${souvenirModel.descriptionSouvenir}</td>
		<td>
			<button type="button" id="btn-edit"
				class="btn btn-success btn-xs btn-edit"
				value="${souvenirModel.idSouvenir}">Edit</button>
			<button type="button" id="btn-delete"
				class="btn btn-danger btn-xs btn-delete"
				value="${souvenirModel.idSouvenir}">Delete</button>
		</td>
	</tr>

</c:forEach>