<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-souvenir">
	<input type="hidden" id="process" name="process" value="create" />
	<div class="form-horizontal">
		<h1>Add Souvenir</h1>

		<div class="form-group">
			<label class="control-label col-md-3">Souvenir Code</label>
			<div class="col-md-6">
				<input type="hidden" id="codeSouvenir" name="codeSouvenir"
					value="${codeSouvenirGenerator}"> <input type="text"
					class="form-input" id="codeSouvenirDisplay"
					name="codeSouvenirDisplay" oninput="setCustomValidity('')"
					disabled="disabled" value="${codeSouvenirGenerator}" />
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Souvenir Name</label>
			<div class="col-md-6">
				<input type="text" class="form-input" id="nameSouvenir"
					name="nameSouvenir" oninput="setCustomValidity('')" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Unit Name</label>
			<div class="col-md-6">
				<select class="form-control" id="idUnit" name="idUnit">
					<c:forEach var="unit" items="${unitList}">
					<option value="${unit.idUnit }">"${unit.nameUnit }"</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Description</label>
			<div class="col-md-6">
				<textarea rows="5" cols="50" class="form-input"
					id="descriptionSouvenir" name="descriptionSouvenir"
					oninput="setCustomValidity('')"></textarea>
			</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-success" id="btn-save"
				onclick="validasiInput();">Save</button>
		</div>
	</div>

</form>

<script>
function validasiInput() {
	var nameSouvenir = document.getElementById("nameSouvenir");

	if (nameSouvenir.value == "") {
		nameSouvenir.setCustomValidity("Souvenir Name Could Not Be Empty");
	}else {

	}
}
</script>