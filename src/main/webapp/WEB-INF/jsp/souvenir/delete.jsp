<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-souvenir">
	<input type="hidden" id="process" name="process" value="delete" />
	<div class="form-horizontal">
		<h1>Add Souvenir</h1>
		<input type="hidden" id="idSouvenir" name="idSouvenir"
			value="${souvenirModel.idSouvenir}" />

		<div class="form-group">
			<label class="control-label col-md-3">Souvenir Code</label>
			<div class="col-md-6">
				<input type="hidden" id="codeSouvenir" name="codeSouvenir"
					value="${souvenirModel.codeSouvenir}"> <input type="text"
					class="form-input" id="codeSouvenirDisplay"
					name="codeSouvenirDisplay" oninput="setCustomValidity('')"
					disabled="disabled" value="${souvenirModel.codeSouvenir}" />
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Souvenir Name</label>
			<div class="col-md-6">
				<input type="hidden" id="nameSouvenir" name="nameSouvenir"
					value="${souvenirModel.nameSouvenir}" /> <input type="text"
					class="form-input" id="nameSouvenirDisplay"
					name="nameSouvenirDisplay" oninput="setCustomValidity('')"
					value="${souvenirModel.nameSouvenir}" disabled="disabled" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Unit Name</label>
			<div class="col-md-6">
			<input type="hidden" id="idUnit" name="idUnit" value ="${unit.idUnit}"/>
				<select class="form-control" id="idUnitDisplay" name="idUnitDisplay" disabled="disabled">
					<c:forEach var="unit" items="${unitList}">
					<option value="${unit.idUnit }">"${unit.nameUnit }"</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Description</label>
			<div class="col-md-6">
				<input type="hidden" id="descriptionSouvenir"
					name="descriptionSouvenir"
					value="${souvenirModel.descriptionSouvenir}" />
				<textarea rows="5" cols="50" class="form-input"
					id="descriptionSouvenirDisplay" name="descriptionSouvenirDisplay"
					oninput="setCustomValidity('')" disabled="disabled">${souvenirModel.descriptionSouvenir}</textarea>
			</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-danger" id="btn-save"
				onclick="validasiInput();">Save</button>
		</div>
	</div>

</form>

<script>
	function validasiInput() {
		var nameSouvenir = document.getElementById("nameSouvenir");

		if (nameSouvenir.value == "") {
			nameSouvenir.setCustomValidity("Souvenir Name Could Not Be Empty");
		}else {

		}
	}
</script>