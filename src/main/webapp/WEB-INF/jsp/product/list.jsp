<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach var="product" items="${productList }">

	<tr>
		<td>${product.codeProduct}</td>
		<td>${product.nameProduct}</td>
		<td>
			<button type ="button" id="btn-edit" class="btn btn-success btn-xs btn-edit" value="${product.idProduct}"><i class="fa fa-edit"></i></button>
			<button type ="button" id="btn-delete" class="btn btn-danger btn-xs btn-delete" value="${product.idProduct}"><i class="fa fa-trash"></i></button>
			<button type ="button" id="btn-detail" class="btn btn-info btn-xs btn-detail" value="${product.idProduct}"><i class="fa fa-eye"></i></button>
			</td>
	</tr>

</c:forEach>