<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-product">
<input type="hidden" id="process" name="process" value="create"/>

	<div class="form-horizontal">

		<div class="form-group">
			<h1>Add Product</h1>
			
	
			<label class="control-label col-md-3"> *Product Code </label>
			<div class="col-md-6">
			<input type="hidden" id="codeProduct" name="codeProduct" value ="${kodeProductGenerator}"/>
				<input type="text" class="form-input" id="codeProductDisplay"
					name="codeProductDisplay" oninput="setCustomValidity('');" value ="${kodeProductGenerator}" disabled="disabled"  />
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">*Product Name</label>
			<div class="col-md-6">
				<input type="text" id="nameProduct" name="nameProduct"
					oninput="setCustomValidity('');" class="form-control"
					required="required">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3"> Description Product</label>
			<div class="col-md-6">
				<input type="text" id="descriptionProduct" name="descriptionProduct"
					oninput="setCustomValidity('');" class="form-control"
					required="required">
			</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-success" id="btn_save"
				onclick="validasiInput();">Save</button>
		</div>

	</div>

</form>
<script>

function validasiInput() {
	var codeProduct = document.getElementById("codeProduct")
	var nameProduct = document.getElementById("nameProduct")
		
	if (codeProduct.value=="") {
		codeProduct.setCustomValidity("Product Code tidak boleh kosong");
		
	}else if (nameProduct.value=="") {
		nameProduct.setCustomValidity("Product Name tidak boleh kosong");
	
	} else{
		
	}

	
}

</script>