<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="panel" style="background: white; margin-top: 40px; min-height: 620px">
<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">List Menu</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right">
				<i class="fa fa-plus"></i>Add
			</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table" id="tbl_menu">
			<thead>
				<tr>
					<th>Code</th>
					<th>Menu Name</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody id="list-data-menu">

			</tbody>
		</table>
	</div>
</div>
</div>
<!-- Modal Input itu untuk pop up -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Menu</h4>
			</div>
			<div class="modal-body">
				<!-- isinya adalah add.jsp -->
			</div>
		</div>
	</div>
</div>
<!-- Akhir Modal Input itu untuk pop up -->

<script>

$(function(){
	$('#tbl_menu').DataTable({
		searching:true
	});
});

loadData();

function loadData() {
	$.ajax({
		url:'menu/list.html',
		type:'get',
		dataType:'html',
		success:function(data){
			$('#list-data-menu').html(data);
		}
	});
}

/* documen ready functionnya untuk ajax ketika suatu button diklik */
$(document).ready(function(){
	
	$('#modal-input').on('submit','#form-menu',function(){
		$.ajax({
			url:'menu/save.json', /* jsaon untuk kirim data ke controlerr */
			type:'get',
			data:$(this).serialize(),
			dataType:'json',
			success: function(data){
				if (data.process=='create') {
					/* jika button submit diklik maka  pop up akan hide */
					$("#modal-input").modal('hide');
					alert('Data Saved! New menu has been add with code' +data.menuModel.codeMenu);
					loadData();
				}else if (data.process=='update') {
					$("#modal-input").modal('hide');
					alert('Data Menu has been updated!');
					loadData();
				}else if (data.process=='delete') {
					$("#modal-input").modal('hide');
					alert('Data Deleted! Data menu with code '+data.menuModel.codeMenu+' has been delete');
					loadData();
				}else {
					
				}
			}
		});
		return false;
	});
	
	/* btn-add onclick fungsinya untuk ajax ketika button add diklik */
	$('#btn-add').on('click',function(){
		/* ajax adalah suatu fungi/method di jsp/ front-end */
		$.ajax({
			url:'menu/add.html',
			type:'get',
			dataType:'html',
			success: function(data){
				/* jika button addnya diklik maka akan muncul pop up show */
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
			
		});
		/* ajax adalah suatu fungi/method di jsp/ front-end */
	});
	/* akhir btn-add onclick fungsinya untuk ajax ketika button add diklik */
	
	/* btn-add onclick fungsinya untuk ajax ketika button add diklik */
	$('#list-data-menu').on('click','#btn-edit',function(){
		var vId = $(this).val();
		/* ajax adalah suatu fungi/method di jsp/ front-end */
		$.ajax({
			url:'menu/edit.html',
			type:'get',
			dataType:'html',
			data:{idMenu:vId},
			success: function(data){
				/* jika button addnya diklik maka akan muncul pop up show */
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
			
		});
		/* ajax adalah suatu fungi/method di jsp/ front-end */
	});
	/* akhir btn-add onclick fungsinya untuk ajax ketika button add diklik */

	$('#list-data-menu').on('click','#btn-delete',function(){
		var vId = $(this).val();
		/* ajax adalah suatu fungi/method di jsp/ front-end */
		$.ajax({
			url:'menu/delete.html',
			type:'get',
			dataType:'html',
			data:{idMenu:vId},
			success: function(data){
				/* jika button addnya diklik maka akan muncul pop up show */
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
			
		});
		/* ajax adalah suatu fungi/method di jsp/ front-end */
	});
	
	$('#list-data-menu').on('click','#btn-detail',function(){
		var vId = $(this).val();
		/* ajax adalah suatu fungi/method di jsp/ front-end */
		$.ajax({
			url:'menu/detail.html',
			type:'get',
			dataType:'html',
			data:{idMenu:vId},
			success: function(data){
				/* jika button addnya diklik maka akan muncul pop up show */
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
			
		});
		/* ajax adalah suatu fungi/method di jsp/ front-end */
	});

	
})
/* akhir document ready function fungsinya untuk ajax */

</script>