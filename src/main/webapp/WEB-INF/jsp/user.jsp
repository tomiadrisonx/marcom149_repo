<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="panel"
	style="background: white; margin-top: 40px; min-height: 620px">
	<div class="box box-info">
		<div class="box-header">
			<h3 class="box-title">List Data User</h3>
			<div class="box-tools">
				<button type="button" id="btn-add"
					class="btn btn-primary pull-right">
					<i class="fa fa-plus"></i>Add User
				</button>
			</div>
		</div>
		<div class="box-body">
			<table class="table" id="tbl_user">
				<thead>
					<tr>
						<th>Employee</th>
						<th>Role</th>
						<th>Company</th>
						<th>Username</th>
						<th>Created Date</th>
						<th>Created By</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="list-data-user">

				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- Awal Modal Input untuk Pop Up-->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<!-- isinya adalah add.jsp -->
			</div>
		</div>
	</div>
</div>
<!-- Akhir Modal Input untuk Pop Up-->

<script>

	/*ajax untuk list*/
	$(function() {
		$('#tbl_user').DataTable({
			searching : true
		});
	});

	loadData();

	function loadData() {
		$.ajax({
			url : 'user/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$('#list-data-user').html(data);
			}
		});
	}
	/*akhir ajax untuk list*/
	
	/* awal document ready funtion untuk ajax ketika suatu button di klik, ajax turunan dari javascript */
	$(document).ready(function() {

		$('#modal-input').on('submit', '#form-user', function() {
			$.ajax({
				url : 'user/save.json', /*json untuk kirim data ke kontroller*/
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(data) {
					if (data.process=='create') {
						/* jika button submit diklik,maka pop up akan hide */
						$("#modal-input").modal('hide');
						alert('Data Saved! New User has been added with username ' +data.userModel.username);						
						loadData();/*untuk refresh listnya*/
					}else if (data.process=='update') {
						$("#modal-input").modal('hide');
						alert('Data Updated');
						loadData();
					}else if (data.process=='delete') {
						$("#modal-input").modal('hide');
						alert('Data Deleted! Data User with username '+data.userModel.username+' has been deleted');
						loadData();
					}else {
						
					}
					
				}

			});
			return false;
		});

		/*btn-add onclick fungsinya untuk ajax ketika btn-add diklik*/
		$('#btn-add').on('click', function() {
			/*ajax adalah suatu fungsi/method di jsp/frontend*/
			$.ajax({
				url : 'user/add.html',
				type : 'get',
				dataType : 'html',
				success : function(data) {
					/* jika button add diklik,maka akan muncul pop up/show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}

			});
			/* akhir ajax adalah suatu fungsi/method di jsp/frontend*/

		});
		/*akhir btn-add onclick fungsinya untuk ajax ketika btn-add diklik*/
		
		//edit
		/*btn-edit onclick fungsinya untuk ajax ketika btn-edit diklik*/
		$('#list-data-user').on('click','#btn-edit',function(){
			var vId = $(this).val();
			/*ajax adalah suatu fungsi/method di jsp/frontend*/
			$.ajax({
				url:'user/edit.html',
				type:'get',
				dataType:'html',
				data:{idUser:vId},
				success:function(data){
					/* jika button edit diklik,maka akan muncul pop up/show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
				
			});
			/* akhir ajax adalah suatu fungsi/method di jsp/frontend*/
			
		});
		/*akhir btn-edit */
		
		//delete
		/*btn-delete */
		$('#list-data-user').on('click','#btn-delete',function(){
			var vId = $(this).val();
			/*ajax adalah suatu fungsi/method di jsp/frontend*/
			$.ajax({
				url:'user/delete.html',
				type:'get',
				dataType:'html',
				data:{idUser:vId},
				success:function(data){
					/* jika button edit diklik,maka akan muncul pop up/show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
				
			});
			/* akhir ajax adalah suatu fungsi/method di jsp/frontend*/
			
		});
		/*akhir btn-delete */
		
		//detail
		$('#list-data-user').on('click','#btn-detail',function(){
			var vId = $(this).val();
			/*ajax adalah suatu fungsi/method di jsp/frontend*/
			$.ajax({
				url:'user/detail.html',
				type:'get',
				dataType:'html',
				data:{idUser:vId},
				success:function(data){
					/* jika button edit diklik,maka akan muncul pop up/show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
				
			});
			/* akhir ajax adalah suatu fungsi/method di jsp/frontend*/
			
		});
		/*akhir btn-detail */

	});
	/*akhir document ready funtion untuk ajax ketika suatu button di klik*/
</script>