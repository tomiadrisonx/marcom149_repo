<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="panel"
	style="background: white; margin-top: 40px; min-height: 620px">
	<div class="box box-info">
		<div class="box-header">
			<h3 class="box-title">Data Request</h3>
			<div class="box-tools">
				<button type="button" id="btn-add"
					class="btn btn-primary pull-right">
					<i class="fa fa-plus"></i>Tambah Request
				</button>
			</div>
		</div>
		<div class="box-body">
			<table class="table" id="tbl_request">
				<thead>
					<tr>
						<th>Request Code</th>
						<th>Request Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="list-data-request">

				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- Modal input itu untuk pop up -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Data Request</h4>
			</div>
			<div class="modal-body">
				<!-- isinya adalah add.jsp -->
			</div>
		</div>
	</div>
</div>
