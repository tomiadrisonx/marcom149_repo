<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="panel"
	style="background: white; margin-top: 40px; min-height: 620px">
	<div class="box box-info">
		<div class="box-header">
			<h3 class="box-title">Data Company</h3>
			<div class="box-tools">
				<button type="button" id="btn-add"
					class="btn btn-primary pull-right">
					<i class="fa fa-plus"></i>  Add Company
				</button>
			</div>
		</div>
		<div class="box-body">
		<table class="table" id="tbl-company">
			<thead>
				<tr>
					
					<th>Company Code</th>
					<th>Company Name</th>
					<th>Created By</th>
					<th>Created Date</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-company">

			</tbody>
		</table>
	</div>
	</div>
</div>

<!-- Modal modal untuk popup -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content"> 
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Data Company</h4>
			</div>
			<div class="modal-body">
				<!-- isinya adalah add.jsp -->
			</div>
		</div>
	</div>
</div>
<!-- Modal Input untuk pop up-->

<script>

/* ajax untuk list */
$(function(){
	
	$('#tbl-company').DataTable({
		
		searching:true
		
	});
	
});

loadData();

	function loadData() {
		$.ajax({
			
			url:'company/list.html',
			type:'get',
			dataType:'html',
			success:function(data){
				$('#list-data-company').html(data);
			}
			
		});
	}
	/* ajax untuk list */
	
	/*document ready function fungsinya untuk ajax ketika suatu button diklik*/
	$(document).ready(function(){
		
		$('#modal-input').on('submit','#form-company', function(){
			
			$.ajax({
				url:'company/save.json', /* json untuk kirim data ke controller */
				type:'get',
				dataType:'json',
				data : $(this).serialize(),
				success:function(data){
					
					if (data.process == 'create') {
						/* jika button submit diklik maka pop akan hide */
						alert('Data Saved! New Company has been added with code ' +data.companyModel.codeCompany+ ' .');
						$("#modal-input").modal('hide');
						loadData();
						
					}else if (data.process == 'update') {
						alert('Data Company Berhasi Diubah');
						$("#modal-input").modal('hide');
						loadData();
					}else if (data.process == 'delete') {
						alert('Data Deleted! Data Company with code ' +data.companyModel.codeCompany+ ' has been deleted.');
						$("#modal-input").modal('hide');
						loadData();
					}else {
						
					}
				}
				
			});
			return false;
			
		});
		
		
		/* btn add onclick fungsinya untuk ajax ketika button add diklik */
		$('#btn-add').on('click',function(){
			/* ajax adalah suat fungsi atau method di jsp / front end */
			$.ajax({
				url:'company/add.html',
				type:'get',
				dataType:'html',
				success:function(data){
					/* jika button add di klil maka akan muncul pop up show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					
				}
				
			});
			/* akhir ajax adalah suat fungsi atau method di jsp / front end */
		});
		/* akhir btn add onclick fungsinya untuk ajax ketika button add diklik */
		
		/* btn edit onclick fungsinya untuk ajax ketika button add diklik */
		$('#list-data-company').on('click','#btn-edit',function(){
			/* ajax adalah suat fungsi atau method di jsp / front end */
			var vId = $(this).val();
			$.ajax({
				url:'company/edit.html',
				type:'get',
				dataType:'html',
				data:{idCompany:vId},
				success:function(data){
					/* jika button add di klil maka akan muncul pop up show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					
				}
				
			});
			/* akhir ajax adalah suat fungsi atau method di jsp / front end */
		});
		/* akhir btn edit onclick fungsinya untuk ajax ketika button add diklik */
		
		/* btn delete onclick fungsinya untuk ajax ketika button add diklik */
		$('#list-data-company').on('click','#btn-delete',function(){
			/* ajax adalah suat fungsi atau method di jsp / front end */
			var vId = $(this).val();
			$.ajax({
				url:'company/delete.html',
				type:'get',
				dataType:'html',
				data:{idCompany:vId},
				success:function(data){
					/* jika button add di klil maka akan muncul pop up show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					
				}
				
			});
			/* akhir ajax adalah suat fungsi atau method di jsp / front end */
		});
		/* akhir btn delete onclick fungsinya untuk ajax ketika button add diklik */
		
		/* btn delete onclick fungsinya untuk ajax ketika button add diklik */
		$('#list-data-company').on('click','#btn-detail',function(){
			/* ajax adalah suat fungsi atau method di jsp / front end */
			var vId = $(this).val();
			$.ajax({
				url:'company/detail.html',
				type:'get',
				dataType:'html',
				data:{idCompany:vId},
				success:function(data){
					/* jika button add di klil maka akan muncul pop up show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					
				}
				
			});
			/* akhir ajax adalah suat fungsi atau method di jsp / front end */
		});
		/* akhir btn delete onclick fungsinya untuk ajax ketika button add diklik */
	});
	/* akhir document ready function fungsinya untuk ajax ketika suatu button diklik*/
</script>