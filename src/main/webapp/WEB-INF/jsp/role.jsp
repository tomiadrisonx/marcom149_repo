<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="panel"
	style="background: white; margin-top: 40px; min-height: 620px">
	<div class="box box-info">
		<div class="box-header">
			<h3 class="box-title">Role Data</h3>
			<div class="box-tools">
				<button type="button" id="btn-add"
					class="btn btn-primary pull-right">
					<i class="fa fa-plus"></i>Add Role
				</button>
			</div>
		</div>
		<div class="box-body">
			<table class="table" id="tbl_role">
				<thead>
					<tr>
						<th>Role Code</th>
						<th>Role Name</th>
						<th>Description</th>
						<th>Created Date</th>
						<th>Created By</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="list-data-role">

				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- Awal Modal Input untuk Pop Up-->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>				
			</div>
			<div class="modal-body">
				<!-- isinya adalah add.jsp -->
			</div>
		</div>
	</div>
</div>
<!-- Akhir Modal Input untuk Pop Up-->

<script>

	/*ajax untuk list*/
	$(function() {
		$('#tbl_role').DataTable({
			searching : true
		});
	});

	loadData();

	function loadData() {
		$.ajax({
			url : 'role/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$('#list-data-role').html(data);
			}
		});
	}
	/*akhir ajax untuk list*/
	
	/* awal document ready funtion untuk ajax ketika suatu button di klik, ajax turunan dari javascript */
	$(document).ready(function() {

		$('#modal-input').on('submit', '#form-role', function() {
			$.ajax({
				url : 'role/save.json', /*json untuk kirim data ke kontroller*/
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(data) {
					if (data.process=='create') {
						/* jika button submit diklik,maka pop up akan hide */
						$("#modal-input").modal('hide');
						alert('Data Saved! New Role has been added with code ' +data.roleModel.codeRole);						
						loadData();/*untuk refresh listnya*/
					}else if (data.process=='update') {
						$("#modal-input").modal('hide');
						alert('Data Updated');
						loadData();
					}else if (data.process=='delete') {
						$("#modal-input").modal('hide');
						alert('Data Deleted! Data Role with code '+data.roleModel.codeRole+' has been delete');
						loadData();
					}else {
						
					}
					
				}

			});
			return false;
		});

		/*btn-add onclick fungsinya untuk ajax ketika btn-add diklik*/
		$('#btn-add').on('click', function() {
			/*ajax adalah suatu fungsi/method di jsp/frontend*/
			$.ajax({
				url : 'role/add.html',
				type : 'get',
				dataType : 'html',
				success : function(data) {
					/* jika button add diklik,maka akan muncul pop up/show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}

			});
			/* akhir ajax adalah suatu fungsi/method di jsp/frontend*/

		});
		/*akhir btn-add onclick fungsinya untuk ajax ketika btn-add diklik*/
		
		//edit
		/*btn-edit onclick fungsinya untuk ajax ketika btn-edit diklik*/
		$('#list-data-role').on('click','#btn-edit',function(){
			var vId = $(this).val();
			/*ajax adalah suatu fungsi/method di jsp/frontend*/
			$.ajax({
				url:'role/edit.html',
				type:'get',
				dataType:'html',
				data:{idRole:vId},
				success:function(data){
					/* jika button edit diklik,maka akan muncul pop up/show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
				
			});
			/* akhir ajax adalah suatu fungsi/method di jsp/frontend*/
			
		});
		/*akhir btn-edit */
		
		//delete
		/*btn-delete */
		$('#list-data-role').on('click','#btn-delete',function(){
			var vId = $(this).val();
			/*ajax adalah suatu fungsi/method di jsp/frontend*/
			$.ajax({
				url:'role/delete.html',
				type:'get',
				dataType:'html',
				data:{idRole:vId},
				success:function(data){
					/* jika button edit diklik,maka akan muncul pop up/show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
				
			});
			/* akhir ajax adalah suatu fungsi/method di jsp/frontend*/
			
		});
		/*akhir btn-delete */
		
		//detail
		/*btn-delete */
		$('#list-data-role').on('click','#btn-detail',function(){
			var vId = $(this).val();
			/*ajax adalah suatu fungsi/method di jsp/frontend*/
			$.ajax({
				url:'role/detail.html',
				type:'get',
				dataType:'html',
				data:{idRole:vId},
				success:function(data){
					/* jika button edit diklik,maka akan muncul pop up/show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
				
			});
			/* akhir ajax adalah suatu fungsi/method di jsp/frontend*/
			
		});
		/*akhir btn-detail */

	});
	/*akhir document ready funtion untuk ajax ketika suatu button di klik*/
</script>