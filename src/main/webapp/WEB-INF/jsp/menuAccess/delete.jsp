<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-menuAccess">
	<input type="hidden" id="process" name="process" value="delete"/>
	<div class="form-horizontal">
		<h3>Delete Menu Access</h3>
		<input type="hidden" id="idMenuAccess" name="idMenuAccess" value="${menuAccessModel.idMenuAccess}"/>
		<div class="form-group">
			<label Class="control-label col-md-6">*Role Code</label>
			<div class="col-md-6">
			<input type="hidden" id="idRole" name="idRole" value="${menuAccessModel.idRole}"/>
				<select id="idRoleDisplay" name="idRoleDisplay" class="form_input" disabled="disabled">
				<c:forEach var="roleModel" items="${roleModelList}">
						<option  value="${roleModel.idRole}">${roleModel.nameRole}</option>
				</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label Class="control-label col-md-6">*Menu Access</label>
			<div class="col-md-6">
			<input type="hidden" id="idMenu" name="idMenu" value="${menuAccessModel.idMenu}"/>
			<select id="idMenuDisplay" name="idMenuDisplay" class="form_input" disabled="disabled">
				<c:forEach var="menuModel" items="${menuModelList}">
						<option value="${menuModel.idMenu}">${menuModel.controllerName}</option>
				</c:forEach>
			</select>
			</div>
		</div>
		
		<div class="modal-footer">
		Anda yakin hapus data ini?
			<button type="submit" class="btn btn-success" id="btn-save">Ya, hapus</button>
		</div>
		
	</div>

</form>
