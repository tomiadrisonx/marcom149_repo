<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form method="get" id="form-menuAccess">
	<input type="hidden" id="process" name="process" value="update"/>
	<div class="form-horizontal">
		<h3>Edit Menu Access</h3>
		<input type="hidden" id="idMenuAccess" name="idMenuAccess" value="${menuAccessModel.idMenuAccess}"/>
		<div class="form-group">
			<label Class="control-label col-md-6">*Role Code</label>
			<div class="col-md-6">
		
			<select id="idRole" name="idRole" class="form-input">
				<c:forEach var="roleModel" items="${roleModelList}">
						<option value="${roleModel.idRole}">${roleModel.nameRole}</option>
				</c:forEach>
			</select>
			</div>
		</div>
		
		<div class="form-group">
			<label Class="control-label col-md-6">*Menu Access</label>
			<div class="col-md-6">
			<select id="idMenu" name="idMenu" class="form-input">
				<c:forEach var="menuModel" items="${menuModelList}">
						<option value="${menuModel.idMenu}">${menuModel.controllerName}</option>
				</c:forEach>
			</select>
			</div>
		</div>
		
		<div class="modal-footer">
			<button type="submit" class="btn btn-success" id="btn-save">Save</button>
		</div>
		
	</div>

</form>
