<!-- untuk looping -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="panel"
	style="background: white; margin-top: 40px; min-height: 620px">
	<div class="box box-info">
		<div class="box-header">
			<h3 class="box-title">Data Approval</h3>
			<div class="box-tools">
				<button type="button" id="btn-add"
					class="btn btn-primary pull-right">
					<i class="fa fa-plus"></i>Tambah Approval
				</button>
			</div>
		</div>
		<div class="box-body">
			<table class="table" id="tbl_approval">
				<thead>
					<tr>
						<th>Approval Code</th>
						<th>Approval Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="list-data-approval">

				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- Modal input itu untuk pop up -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Data Approval</h4>
			</div>
			<div class="modal-body">
				<!-- isinya adalah add.jsp -->
			</div>
		</div>
	</div>
</div>
<!-- Akhir Modal input itu untuk pop up -->
<script>
	$(function() {
		$('#tbl_approval').DataTable({
			searching : true

		});
	});

	loadData();

	function loadData() {
		$.ajax({
			url : 'approval/list.html',
			type : 'get',
			dataType : 'html',
			data : $(this).serialize(),
			success : function(data) {

				$('#list-data-approval').html(data);
			}

		});
	}
	/* document ready function fgs nya utk ajax ketika suatu button diklik */
	$(document).ready(function() {

		/* btn-add onClick fgs nya utk ajax ketika button add di click */
		$('#modal-input').on('submit', '#form-approval', function() {

			$.ajax({
				url : 'approval/save.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(data) {
					/* jika button submit nya di klik maka pop up akan hide */

					if (data.process == "create") {
						/* jika button submit nya di klik maka pop up akan hide */
						$("#modal-input").modal('hide');
						alert('Data Approval berhasil disimpan');
						loadData();

					} else if (data.process == "update") {
						/* jika button submit nya di klik maka pop up akan hide */
						$("#modal-input").modal('hide');
						alert('Data Approval berhasil di update');
						loadData();
					} else if (data.process == "delete") {
						/* jika button submit nya di klik maka pop up akan hide */
						$("#modal-input").modal('hide');
						alert('Data Approval berhasil di delete');
						loadData();
					} else {

					}
				}

			});
			return false;

		});

		// Tambah popup
		/* btn-add onClick fgs nya utk ajax ketika button add di click */
		$('#btn-add').on('click', function() {
			/* ajax adalah suaatu fungsi atau method di jsp/front end */
			$.ajax({

				url : 'approval/add.html',
				type : 'get',
				dataType : 'html',
				success : function(data) {
					/* jika button add nya di klik maka akan muncul pop up show */
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');

				}
			});
			/* akhir ajax adalah suaatu fungsi atau method di jsp/front end */

		})
		/* akhir btn-add onClick fgs nya utk ajax ketika button add di click */
	})
	/* akhir document ready function fgs nya utk ajax ketika suatu button diklik */
	//Edit
	//function untuk button edit
	$('#list-data-approval').on('click', '#btn-edit', function() {
		var vId = $(this).val();
		$.ajax({
			url : 'approval/edit.html',
			type : 'get',
			data : {
				idApproval : vId
			},
			dataType : 'html', //untuk menampilkan halaman
			success : function(data) {
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
	});

	//function untuk button delete
	$('#list-data-approval').on('click', '#btn-delete', function() {
		var vId = $(this).val();
		$.ajax({
			url : 'approval/delete.html',
			type : 'get',
			data : {
				idApproval : vId
			},
			dataType : 'html', //untuk menampilkan halaman
			success : function(data) {
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
	});
	//function untuk button delete
	$('#list-data-approval').on('click', '#btn-detail', function() {
		var vId = $(this).val();
		$.ajax({
			url : 'approval/detail.html',
			type : 'get',
			data : {
				idApproval : vId
			},
			dataType : 'html', //untuk menampilkan halaman
			success : function(data) {
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
	});
</script>

