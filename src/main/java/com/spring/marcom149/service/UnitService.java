package com.spring.marcom149.service;

import java.util.List;

import com.spring.marcom149.model.UnitModel;

public interface UnitService {
	
	public void create(UnitModel unit) throws Exception;
	
	public void update(UnitModel unit) throws Exception;
	
	public void delete(UnitModel unit) throws Exception;
	
	public List<UnitModel> list() throws Exception;
	
/*transaksi searchByKode*/
	
	public UnitModel searchByKode (String codeUnit) throws Exception;
	
	
	public UnitModel searchById (Integer idUnit) throws Exception;
	

}
