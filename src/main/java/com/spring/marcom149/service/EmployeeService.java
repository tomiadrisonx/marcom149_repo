package com.spring.marcom149.service;

import java.util.List;

import com.spring.marcom149.model.EmployeeModel;

public interface EmployeeService {

public void create (EmployeeModel employeeModel) throws Exception;
	
	public void update (EmployeeModel employeeModel) throws Exception;
	
	public void delete (EmployeeModel employeeModel) throws Exception;
	
	public List<EmployeeModel>list() throws Exception;
	
	/*transaksi searchByKode*/
	public EmployeeModel searchByKode(String codeEmployee) throws Exception;
	
	/*transaksi searchById*/
	public EmployeeModel searchById(Integer idEmployee) throws Exception;


}
