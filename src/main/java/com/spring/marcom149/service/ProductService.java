package com.spring.marcom149.service;

import java.util.List;

import com.spring.marcom149.model.ProductModel;

public interface ProductService {
	
	public void create(ProductModel product) throws Exception;
	
	public void update(ProductModel product) throws Exception;
	
	public void delete(ProductModel product) throws Exception;
	
	public List<ProductModel> list() throws Exception;
	
/*transaksi searchByKode*/
	
	public ProductModel searchByKode (String codeProduct) throws Exception;
	
	
	public ProductModel searchById (Integer idProduct) throws Exception;
	

}
