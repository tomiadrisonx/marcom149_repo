package com.spring.marcom149.service;

import java.util.List;

import com.spring.marcom149.model.CompanyModel;


public interface CompanyService {
	
public void create (CompanyModel companyModel) throws Exception;
	
	public void update (CompanyModel companyModel) throws Exception;
	
	public void delete (CompanyModel companyModel) throws Exception;
	
	public List<CompanyModel>list() throws Exception;
	
	/*transaksi searchByKode*/
	public CompanyModel searchByKode(String codeCompany) throws Exception;
	
	/*transaksi searchById*/
	public CompanyModel searchById(Integer idCompany) throws Exception;


}
