package com.spring.marcom149.service;

import java.util.List;

import com.spring.marcom149.model.RoleModel;

public interface RoleService {
	
	public void create(RoleModel roleModel) throws Exception;
	
	public void update(RoleModel roleModel) throws Exception;
	
	public void delete(RoleModel roleModel) throws Exception;
	
	public List<RoleModel> list() throws Exception;
	
	public RoleModel searcByCode(String codeRole) throws Exception;
	
	public RoleModel searchById(Integer idRole) throws Exception;

}
