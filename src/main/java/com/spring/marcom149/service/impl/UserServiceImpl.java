package com.spring.marcom149.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.dao.UserDao;
import com.spring.marcom149.model.UserModel;
import com.spring.marcom149.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserDao userDao;

	@Override
	public void create(UserModel userModel) throws Exception {
		// TODO Auto-generated method stub
		this.userDao.create(userModel);
		
	}

	@Override
	public void update(UserModel userModel) throws Exception {
		// TODO Auto-generated method stub
		this.userDao.update(userModel);
		
	}

	@Override
	public void delete(UserModel userModel) throws Exception {
		// TODO Auto-generated method stub
		this.userDao.delete(userModel);
		
	}

	@Override
	public List<UserModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.userDao.list();
	}

	@Override
	public UserModel searchById(Integer idUser) throws Exception {
		// TODO Auto-generated method stub
		return this.userDao.searchById(idUser);
	}

	@Override
	public UserModel searchByUsernamePassword(String username, String password) throws Exception {
		// TODO Auto-generated method stub
		return this.userDao.searchByUsernamePassword(username, password);
	}

}
