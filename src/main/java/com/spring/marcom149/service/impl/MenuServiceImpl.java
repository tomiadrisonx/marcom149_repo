package com.spring.marcom149.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.dao.MenuDao;
import com.spring.marcom149.model.MenuModel;
import com.spring.marcom149.service.MenuService;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {
	
	@Autowired
	private MenuDao menuDao;

	@Override
	public void create(MenuModel menuModel) throws Exception {
		// TODO Auto-generated method stub
		this.menuDao.create(menuModel);
	}

	@Override
	public void update(MenuModel menuModel) throws Exception {
		// TODO Auto-generated method stub
		this.menuDao.update(menuModel);
	}

	@Override
	public void delete(MenuModel menuModel) throws Exception {
		// TODO Auto-generated method stub
		this.menuDao.delete(menuModel);
	}

	@Override
	public List<MenuModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.menuDao.list();
	}

	@Override
	public MenuModel searchByCode(String codeMenu) throws Exception {
		// TODO Auto-generated method stub
		return this.menuDao.searchByCode(codeMenu);
	}

	@Override
	public MenuModel searchById(Integer idMenu) throws Exception {
		// TODO Auto-generated method stub
		return this.menuDao.searchById(idMenu);
	}

	@Override
	public List<MenuModel> getAllMenuTree() throws Exception {
		// TODO Auto-generated method stub
		return this.menuDao.getAllMenuTree();
	}

	@Override
	public List<MenuModel> getAllMenuTreeByRole(Integer idRole) throws Exception {
		// TODO Auto-generated method stub
		return this.menuDao.getAllMenuTreeByRole(idRole);
	}

}
