package com.spring.marcom149.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.marcom149.dao.ProductDao;
import com.spring.marcom149.model.ProductModel;
import com.spring.marcom149.service.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductDao productDao;

	@Override
	public void create(ProductModel product) throws Exception {
		// TODO Auto-generated method stub
		this.productDao.create(product);
		
	}

	@Override
	public void update(ProductModel product) throws Exception {
		// TODO Auto-generated method stub
		this.productDao.update(product);
		
	}

	@Override
	public void delete(ProductModel product) throws Exception {
		// TODO Auto-generated method stub
		this.productDao.delete(product);
		
	}

	@Override
	public List<ProductModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.productDao.list();
	}
	
	@Override
	public ProductModel searchByKode (String codeProduct) throws Exception {
		// TODO Auto-generated method stu
		
		return this.productDao.searchByKode(codeProduct);

	}
	@Override
	public ProductModel searchById (Integer idProduct) throws Exception {
		// TODO Auto-generated method stu
		
		return this.productDao.searchById(idProduct);

	}
}
