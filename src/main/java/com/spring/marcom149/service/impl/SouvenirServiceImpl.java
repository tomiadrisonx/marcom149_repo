package com.spring.marcom149.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.dao.SouvenirDao;
import com.spring.marcom149.model.SouvenirModel;
import com.spring.marcom149.service.SouvenirService;

@Service
@Transactional
public class SouvenirServiceImpl implements SouvenirService{
	
	@Autowired
	private SouvenirDao souvenirDao;
	
	@Override
	public void create(SouvenirModel souvenirModel) throws Exception {
		// TODO Auto-generated method stub
		this.souvenirDao.create(souvenirModel);
	}
	
	@Override
	public void update(SouvenirModel souvenirModel) throws Exception {
		// TODO Auto-generated method stub
		this.souvenirDao.update(souvenirModel);
	}

	@Override
	public void delete(SouvenirModel souvenirModel) throws Exception {
		// TODO Auto-generated method stub
		this.souvenirDao.delete(souvenirModel);
	}

	@Override
	public List<SouvenirModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.souvenirDao.list();
	}

	@Override
	public SouvenirModel searchByKode(String codeSouvenir) throws Exception {
		// TODO Auto-generated method stub
		return this.souvenirDao.searchByKode(codeSouvenir);
	}

	@Override
	public SouvenirModel searchById(Integer idSouvenir) throws Exception {
		// TODO Auto-generated method stub
		return this.souvenirDao.searchById(idSouvenir);
	}
}
