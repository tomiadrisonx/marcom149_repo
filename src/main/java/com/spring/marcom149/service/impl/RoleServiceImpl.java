package com.spring.marcom149.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.dao.RoleDao;
import com.spring.marcom149.model.RoleModel;
import com.spring.marcom149.service.RoleService;

@Service
@Transactional
public class RoleServiceImpl implements RoleService{
	
	@Autowired
	private RoleDao roleDao;

	@Override
	public void create(RoleModel roleModel) throws Exception {
		// TODO Auto-generated method stub
		this.roleDao.create(roleModel);
		
	}

	@Override
	public void update(RoleModel roleModel) throws Exception {
		// TODO Auto-generated method stub
		this.roleDao.update(roleModel);
		
	}

	@Override
	public void delete(RoleModel roleModel) throws Exception {
		// TODO Auto-generated method stub
		this.roleDao.delete(roleModel);
		
	}

	@Override
	public List<RoleModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.roleDao.list();
	}

	@Override
	public RoleModel searcByCode(String codeRole) throws Exception {
		// TODO Auto-generated method stub
		return this.roleDao.searcByCode(codeRole);
	}

	@Override
	public RoleModel searchById(Integer idRole) throws Exception {
		// TODO Auto-generated method stub
		return this.roleDao.searchById(idRole);
	}

}
