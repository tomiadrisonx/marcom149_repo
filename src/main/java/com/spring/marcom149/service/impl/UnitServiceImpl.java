package com.spring.marcom149.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.marcom149.dao.UnitDao;
import com.spring.marcom149.model.UnitModel;
import com.spring.marcom149.service.UnitService;

@Service
@Transactional
public class UnitServiceImpl implements UnitService {
	
	@Autowired
	private UnitDao unitDao;

	@Override
	public void create(UnitModel unit) throws Exception {
		// TODO Auto-generated method stub
		this.unitDao.create(unit);
		
	}

	@Override
	public void update(UnitModel unit) throws Exception {
		// TODO Auto-generated method stub
		this.unitDao.update(unit);
		
	}

	@Override
	public void delete(UnitModel unit) throws Exception {
		// TODO Auto-generated method stub
		this.unitDao.delete(unit);
		
	}

	@Override
	public List<UnitModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.unitDao.list();
	}
	
	@Override
	public UnitModel searchByKode (String codeUnit) throws Exception {
		// TODO Auto-generated method stu
		
		return this.unitDao.searchByKode(codeUnit);

	}
	@Override
	public UnitModel searchById (Integer idUnit) throws Exception {
		// TODO Auto-generated method stu
		
		return this.unitDao.searchById(idUnit);

	}
}
