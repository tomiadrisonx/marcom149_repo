package com.spring.marcom149.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.dao.DivisionDao;
import com.spring.marcom149.model.DivisionModel;
import com.spring.marcom149.service.DivisionService;

@Service
@Transactional
public class DivisionServiceImpl implements DivisionService{
	
	@Autowired
	private DivisionDao divisionDao;
	
	@Override
	public void create(DivisionModel divisionModel) throws Exception {
		// TODO Auto-generated method stub
		this.divisionDao.create(divisionModel);
	}
	
	@Override
	public void update(DivisionModel divisionModel) throws Exception {
		// TODO Auto-generated method stub
		this.divisionDao.update(divisionModel);
	}

	@Override
	public void delete(DivisionModel divisionModel) throws Exception {
		// TODO Auto-generated method stub
		this.divisionDao.delete(divisionModel);
	}

	@Override
	public List<DivisionModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.divisionDao.list();
	}

	@Override
	public DivisionModel searchByKode(String codeDivision) throws Exception {
		// TODO Auto-generated method stub
		return this.divisionDao.searchByKode(codeDivision);
	}

	@Override
	public DivisionModel searchById(Integer idDivision) throws Exception {
		// TODO Auto-generated method stub
		return this.divisionDao.searchById(idDivision);
	}
}
