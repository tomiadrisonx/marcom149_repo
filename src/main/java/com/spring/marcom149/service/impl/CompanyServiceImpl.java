package com.spring.marcom149.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.model.CompanyModel;
import com.spring.marcom149.service.CompanyService;
import com.spring.marcom149.dao.CompanyDao;

@Service
@Transactional
public class CompanyServiceImpl implements CompanyService{
	
	@Autowired
	private CompanyDao companyDao;

	@Override
	public void create(CompanyModel companyModel) throws Exception {
		// TODO Auto-generated method stub
		this.companyDao.create(companyModel);
	}

	@Override
	public void update(CompanyModel companyModel) throws Exception {
		// TODO Auto-generated method stub
		this.companyDao.update(companyModel);
	}

	@Override
	public void delete(CompanyModel companyModel) throws Exception {
		// TODO Auto-generated method stub
		this.companyDao.delete(companyModel);
	}

	@Override
	public List<CompanyModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.companyDao.list();
	}

	@Override
	public CompanyModel searchByKode(String codeCompany) throws Exception {
		// TODO Auto-generated method stub
		return this.companyDao.searchByKode(codeCompany);
	}

	@Override
	public CompanyModel searchById(Integer idCompany) throws Exception {
		// TODO Auto-generated method stub
		return this.companyDao.searchById(idCompany);
	}

}
