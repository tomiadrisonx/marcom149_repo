package com.spring.marcom149.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.marcom149.dao.MenuAccessDao;
import com.spring.marcom149.model.MenuAccessModel;
import com.spring.marcom149.service.MenuAccessService;

@Service
@Transactional
public class MenuAccessServiceImpl implements MenuAccessService {

	@Autowired
	private MenuAccessDao menuAccessDao;
	
	@Override
	public void create(MenuAccessModel menuAccessModel) throws Exception {
		// TODO Auto-generated method stub
		this.menuAccessDao.create(menuAccessModel);
	}

	@Override
	public void update(MenuAccessModel menuAccessModel) throws Exception {
		// TODO Auto-generated method stub
		this.menuAccessDao.update(menuAccessModel);
	}

	@Override
	public void delete(MenuAccessModel menuAccessModel) throws Exception {
		// TODO Auto-generated method stub
		this.menuAccessDao.delete(menuAccessModel);
	}

	@Override
	public List<MenuAccessModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.menuAccessDao.list();
	}

	@Override
	public MenuAccessModel searchById(Integer idMenuAccess) throws Exception {
		// TODO Auto-generated method stub
		return this.menuAccessDao.searchById(idMenuAccess);
	}

}
