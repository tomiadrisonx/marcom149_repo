package com.spring.marcom149.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


@Entity
@Table(name = "M_USER")
public class UserModel {
	
	private Integer idUser;
	private String username;
	private String password;
	
	//join table ke role
	private Integer idRole;
	private RoleModel roleModel;
	
	private Integer idEmployee;
	private EmployeeModel EmployeeModel;
	
	private String createdByUser;
	private Date createdDateUser;
	private String updatedByUser;
	private Date updatedDateUser;
	private Integer isDeleteUser;
	
	@Id
	@Column(name="ID_USER")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="M_USER")
	@TableGenerator(name="M_USER",table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue="M_USER_ID", valueColumnName="SEQUENCE_VALUE",
					allocationSize = 1, initialValue = 1)
	public Integer getIdUser() {
		return idUser;
	}
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
	
	@Column(name="USERNAME")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name="PASSWORD")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="ID_ROLE")
	public Integer getIdRole() {
		return idRole;
	}
	public void setIdRole(Integer idRole) {
		this.idRole = idRole;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_ROLE", nullable = false, updatable = false, insertable = false)
	public RoleModel getRoleModel() {
		return roleModel;
	}
	public void setRoleModel(RoleModel roleModel) {
		this.roleModel = roleModel;
	}
	
	@Column(name="ID_EMPLOYEE")
	public Integer getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_EMPLOYEE", nullable = false, updatable = false, insertable = false)
	public EmployeeModel getEmployeeModel() {
		return EmployeeModel;
	}
	public void setEmployeeModel(EmployeeModel employeeModel) {
		EmployeeModel = employeeModel;
	}
	
	@Column(name="CREATED_BY_USER")
	public String getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}
	
	@Column(name="CREATED_DATE_USER")
	public Date getCreatedDateUser() {
		return createdDateUser;
	}
	public void setCreatedDateUser(Date createdDateUser) {
		this.createdDateUser = createdDateUser;
	}
	
	@Column(name="UPDATED_BY_USER")
	public String getUpdatedByUser() {
		return updatedByUser;
	}
	public void setUpdatedByUser(String updatedByUser) {
		this.updatedByUser = updatedByUser;
	}
	
	@Column(name="UPDATED_DATE_USER")
	public Date getUpdatedDateUser() {
		return updatedDateUser;
	}
	public void setUpdatedDateUser(Date updatedDateUser) {
		this.updatedDateUser = updatedDateUser;
	}
	
	@Column(name="IS_DELETE_USER")
	public Integer getIsDeleteUser() {
		return isDeleteUser;
	}
	public void setIsDeleteUser(Integer isDeleteUser) {
		this.isDeleteUser = isDeleteUser;
	}
	
	
	
	
	
	
	
	
	
	

}
