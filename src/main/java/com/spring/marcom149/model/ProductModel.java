package com.spring.marcom149.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "M_PRODUCT")
public class ProductModel {
	
	private Integer idProduct;
	private String codeProduct;
	private String nameProduct;
	private String descriptionProduct;
	
	private String createdByProduct;
	private Date createdDateProduct;
	private String updatedByProduct;
	private Date updatedDateProduct;
	private Integer isDeleteProduct;
	
	
	@Id
	@Column(name="ID_PRODUCT")
	@GeneratedValue(strategy = GenerationType.TABLE, generator="M_PRODUCT")
	@TableGenerator(name="M_PRODUCT", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue ="M_PRODUCT_ID", valueColumnName="SEQUENCE_VALUE",
					allocationSize=1, initialValue=1)
	public Integer getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}
	
	@Column(name="CODE_PRODUCT")
	public String getCodeProduct() {
		return codeProduct;
	}
	public void setCodeProduct(String codeProduct) {
		this.codeProduct = codeProduct;
	}
	
	@Column(name="NAME_PRODUCT")
	public String getNameProduct() {
		return nameProduct;
	}
	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}
	
	@Column(name="DESCRIPTION_PRODUCT")
	public String getDescriptionProduct() {
		return descriptionProduct;
	}
	public void setDescriptionProduct(String descriptionProduct) {
		this.descriptionProduct = descriptionProduct;
	}
	
	@Column(name="CREATED_BY_PRODUCT")
	public String getCreatedByProduct() {
		return createdByProduct;
	}
	public void setCreatedByProduct(String createdByProduct) {
		this.createdByProduct = createdByProduct;
	}
	
	@Column(name="CREATED_DATE_PRODUCT")
	public Date getCreatedDateProduct() {
		return createdDateProduct;
	}
	public void setCreatedDateProduct(Date createdDateProduct) {
		this.createdDateProduct = createdDateProduct;
	}
	
	@Column(name="UPDATE_BY_PRODUCT")
	public String getUpdatedByProduct() {
		return updatedByProduct;
	}
	public void setUpdatedByProduct(String updatedByProduct) {
		this.updatedByProduct = updatedByProduct;
	}
	
	@Column(name="UPDATE_DATE_PRODUCT")
	public Date getUpdatedDateProduct() {
		return updatedDateProduct;
	}
	public void setUpdatedDateProduct(Date updatedDateProduct) {
		this.updatedDateProduct = updatedDateProduct;
	}
	
	@Column(name="IS_DELETE_PRODUCT")
	public Integer getIsDeleteProduct() {
		return isDeleteProduct;
	}
	public void setIsDeleteProduct(Integer isDeleteProduct) {
		this.isDeleteProduct = isDeleteProduct;
	}
	
	
	
		}
