package com.spring.marcom149.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import com.spring.marcom149.model.UnitModel;

@Entity
@Table(name="M_SOUVENIR")
public class SouvenirModel {

	private Integer idSouvenir;
	private String codeSouvenir;
	private String nameSouvenir;
	private String descriptionSouvenir;
	
	/*join table ke Unit*/
	private Integer idUnit;
	private UnitModel unit;
	
	private String createdBySouvenir;
	private Date createdDateSouvenir;
	private String updatedBySouvenir;
	private Date updatedDateSouvenir;
	private Integer isDeleteSouvenir;
	
	@Id
	@Column(name="ID_SOUVENIR")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_SOUVENIR")
	@TableGenerator(name="M_SOUVENIR", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue="M_SOUVENIR_ID", valueColumnName="SEQUENCE_VALUE",
					allocationSize=1, initialValue=1)
	public Integer getIdSouvenir() {
		return idSouvenir;
	}
	public void setIdSouvenir(Integer idSouvenir) {
		this.idSouvenir = idSouvenir;
	}
	
	@Column(name="CODE_SOUVENIR")
	public String getCodeSouvenir() {
		return codeSouvenir;
	}
	public void setCodeSouvenir(String codeSouvenir) {
		this.codeSouvenir = codeSouvenir;
	}
	
	@Column(name="NAME_SOUVENIR")
	public String getNameSouvenir() {
		return nameSouvenir;
	}
	public void setNameSouvenir(String nameSouvenir) {
		this.nameSouvenir = nameSouvenir;
	}
	
	@Column(name="DESCRIPTION_SOUVENIR")
	public String getDescriptionSouvenir() {
		return descriptionSouvenir;
	}
	public void setDescriptionSouvenir(String descriptionSouvenir) {
		this.descriptionSouvenir = descriptionSouvenir;
	}
	
	@Column(name="ID_UNIT")
	public Integer getIdUnit() {
		return idUnit;
	}
	public void setIdUnit(Integer idUnit) {
		this.idUnit = idUnit;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_UNIT", nullable=false, updatable=false, insertable=false)
	public UnitModel getUnit() {
		return unit;
	}
	public void setUnit(UnitModel unit) {
		this.unit = unit;
	}
	@Column(name="CREATED_BY_SOUVENIR")
	public String getCreatedBySouvenir() {
		return createdBySouvenir;
	}
	public void setCreatedBySouvenir(String createdBySouvenir) {
		this.createdBySouvenir = createdBySouvenir;
	}
	
	@Column(name="CREATED_DATE_SOUVENIR")
	public Date getCreatedDateSouvenir() {
		return createdDateSouvenir;
	}
	public void setCreatedDateSouvenir(Date createdDateSouvenir) {
		this.createdDateSouvenir = createdDateSouvenir;
	}
	
	@Column(name="UPDATE_BY_SOUVENIR")
	public String getUpdatedBySouvenir() {
		return updatedBySouvenir;
	}
	public void setUpdatedBySouvenir(String updatedBySouvenir) {
		this.updatedBySouvenir = updatedBySouvenir;
	}
	
	@Column(name="UPDATE_DATE_SOUVENIR")
	public Date getUpdatedDateSouvenir() {
		return updatedDateSouvenir;
	}
	public void setUpdatedDateSouvenir(Date updatedDateSouvenir) {
		this.updatedDateSouvenir = updatedDateSouvenir;
	}
	
	@Column(name="IS_DELETE_SOUVENIR")
	public Integer getIsDeleteSouvenir() {
		return isDeleteSouvenir;
	}
	public void setIsDeleteSouvenir(Integer isDeleteSouvenir) {
		this.isDeleteSouvenir = isDeleteSouvenir;
	}
	
	
}
