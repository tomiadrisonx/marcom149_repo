package com.spring.marcom149.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_MENU_ACCESS")
public class MenuAccessModel {

	private Integer idMenuAccess;
	private Integer idRole;
	private RoleModel roleModel;
	private Integer idMenu;
	private MenuModel menuModel;
	private Integer isDeleteMenuAccess;
	private String createByMenuAccess;
	private Date createDateMenuAccess;
	private String updateByMenuAccess;
	private Date updateDateMenuAccess;
	
	@Id
	@Column(name="ID_MENU_ACCESS")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="M_MENU_ACCESS")
	@TableGenerator(name="M_MENU_ACCESS",table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
	pkColumnValue="M_MENU_ACCESS_ID", valueColumnName="SEQUENCE_VALUE", allocationSize=1,initialValue=1)
	public Integer getIdMenuAccess() {
		return idMenuAccess;
	}
	public void setIdMenuAccess(Integer idMenuAccess) {
		this.idMenuAccess = idMenuAccess;
	}
	@Column(name="ID_ROLE")
	public Integer getIdRole() {
		return idRole;
	}
	public void setIdRole(Integer idRole) {
		this.idRole = idRole;
	}
	@ManyToOne
	@JoinColumn(name="ID_ROLE", nullable = false, updatable = false, insertable = false)
	public RoleModel getRoleModel() {
		return roleModel;
	}
	public void setRoleModel(RoleModel roleModel) {
		this.roleModel = roleModel;
	}
	@Column(name="ID_MENU")
	public Integer getIdMenu() {
		return idMenu;
	}
	public void setIdMenu(Integer idMenu) {
		this.idMenu = idMenu;
	}
	@ManyToOne
	@JoinColumn(name="ID_MENU", nullable = false, updatable = false, insertable = false)
	public MenuModel getMenuModel() {
		return menuModel;
	}
	public void setMenuModel(MenuModel menuModel) {
		this.menuModel = menuModel;
	}
	@Column(name="IS_DELETE_MENU_ACCESS")
	public Integer getIsDeleteMenuAccess() {
		return isDeleteMenuAccess;
	}
	public void setIsDeleteMenuAccess(Integer isDeleteMenuAccess) {
		this.isDeleteMenuAccess = isDeleteMenuAccess;
	}
	@Column(name="CREATE_BY_MENU_ACCESS")
	public String getCreateByMenuAccess() {
		return createByMenuAccess;
	}
	public void setCreateByMenuAccess(String createByMenuAccess) {
		this.createByMenuAccess = createByMenuAccess;
	}
	@Column(name="CREATE_DATE_MENU_ACCESS")
	public Date getCreateDateMenuAccess() {
		return createDateMenuAccess;
	}
	public void setCreateDateMenuAccess(Date createDateMenuAccess) {
		this.createDateMenuAccess = createDateMenuAccess;
	}
	@Column(name="UPDATE_BY_MENU_ACCESS")
	public String getUpdateByMenuAccess() {
		return updateByMenuAccess;
	}
	public void setUpdateByMenuAccess(String updateByMenuAccess) {
		this.updateByMenuAccess = updateByMenuAccess;
	}
	@Column(name="UPDATE_DATE_MENU_ACCESS")
	public Date getUpdateDateMenuAccess() {
		return updateDateMenuAccess;
	}
	public void setUpdateDateMenuAccess(Date updateDateMenuAccess) {
		this.updateDateMenuAccess = updateDateMenuAccess;
	}

}
