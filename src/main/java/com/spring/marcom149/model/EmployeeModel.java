package com.spring.marcom149.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_EMPLOYEE")
public class EmployeeModel {
	
	
	private Integer idEmployee;
	private String codeEmployee;
	private String firstnameEmployee;
	private String lastnameEmployee;
	
	private Integer idCompany;
	private CompanyModel companyModel;
	
	/*private Integer idDivision;
	private DivisionModel divisionModel;*/
	
	private String emailEmployee;
	
	/*private String createdByEmployee;
	private Date createdDateEmployee;
	private String updatedByEmployee;
	private Date updatedDateEmployee;
	private Integer isDeleteEmployee;*/

	@Id
	@Column(name="ID_EMPLOYEE")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="M_EMPLOYEE")
	@TableGenerator(name="M_EMPLOYEE", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue="M_EMPLOYEE_ID", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}
	
	@Column(name="CODE_EMPLOYEE")
	public String getCodeEmployee() {
		return codeEmployee;
	}
	public void setCodeEmployee(String codeEmployee) {
		this.codeEmployee = codeEmployee;
	}
	
	@Column(name="FIRSTNAME_EMPLOYEE")
	public String getFirstnameEmployee() {
		return firstnameEmployee;
	}

	public void setFirstnameEmployee(String firstnameEmployee) {
		this.firstnameEmployee = firstnameEmployee;
	}
	
	@Column(name="LASTNAME_EMPLOYEE")
	public String getLastnameEmployee() {
		return lastnameEmployee;
	}
	public void setLastnameEmployee(String lastnameEmployee) {
		this.lastnameEmployee = lastnameEmployee;
	}
	
	@Column(name="ID_COMPANY")
	public Integer getIdCompany() {
		return idCompany;
	}
	public void setIdCompany(Integer idCompany) {
		this.idCompany = idCompany;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_COMPANY", nullable = false, updatable = false, insertable = false )
	public CompanyModel getCompanyModel() {
		return companyModel;
	}

	public void setCompanyModel(CompanyModel companyModel) {
		this.companyModel = companyModel;
	}
	@Column(name="EMAIL_EMPLOYEE")
	public String getEmailEmployee() {
		return emailEmployee;
	}

	public void setEmailEmployee(String emailEmployee) {
		this.emailEmployee = emailEmployee;
	}
	
	
}
