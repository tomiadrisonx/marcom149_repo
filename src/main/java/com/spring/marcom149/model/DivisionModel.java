package com.spring.marcom149.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_DIVISION")
public class DivisionModel {

	private Integer idDivision;
	private String codeDivision;
	private String nameDivision;
	private String descriptionDivision;
	
	private String createdByDivision;
	private Date createdDateDivision;
	private String updatedByDivision;
	private Date updatedDateDivision;
	private Integer isDeleteDivision;
	
	@Id
	@Column(name="ID_DIVISION")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_DIVISION")
	@TableGenerator(name="M_DIVISION", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue="M_DIVISION_ID", valueColumnName="SEQUENCE_VALUE",
					allocationSize=1, initialValue=1)
	public Integer getIdDivision() {
		return idDivision;
	}
	public void setIdDivision(Integer idDivision) {
		this.idDivision = idDivision;
	}
	
	@Column(name="CODE_DIVISION")
	public String getCodeDivision() {
		return codeDivision;
	}
	public void setCodeDivision(String codeDivision) {
		this.codeDivision = codeDivision;
	}
	
	@Column(name="NAME_DIVISION")
	public String getNameDivision() {
		return nameDivision;
	}
	public void setNameDivision(String nameDivision) {
		this.nameDivision = nameDivision;
	}
	
	@Column(name="DESCRIPTION_DIVISION")
	public String getDescriptionDivision() {
		return descriptionDivision;
	}
	public void setDescriptionDivision(String descriptionDivision) {
		this.descriptionDivision = descriptionDivision;
	}
	
	@Column(name="CREATED_BY_DIVISION")
	public String getCreatedByDivision() {
		return createdByDivision;
	}
	public void setCreatedByDivision(String createdByDivision) {
		this.createdByDivision = createdByDivision;
	}
	
	@Column(name="CREATED_DATE_DIVISION")
	public Date getCreatedDateDivision() {
		return createdDateDivision;
	}
	public void setCreatedDateDivision(Date createdDateDivision) {
		this.createdDateDivision = createdDateDivision;
	}
	
	
	@Column(name="UPDATE_BY_DIVISION")
	public String getUpdatedByDivision() {
		return updatedByDivision;
	}
	public void setUpdatedByDivision(String updatedByDivision) {
		this.updatedByDivision = updatedByDivision;
	}
	
	@Column(name="UPDATE_DATE_DIVISION")
	public Date getUpdatedDateDivision() {
		return updatedDateDivision;
	}
	public void setUpdatedDateDivision(Date updatedDateDivision) {
		this.updatedDateDivision = updatedDateDivision;
	}
	
	@Column(name="IS_DELETE_DIVISION")
	public Integer getIsDeleteDivision() {
		return isDeleteDivision;
	}
	public void setIsDeleteDivision(Integer isDeleteDivision) {
		this.isDeleteDivision = isDeleteDivision;
	}
	
	
	

}
