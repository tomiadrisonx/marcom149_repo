package com.spring.marcom149.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "M_UNIT")
public class UnitModel {
	
	private Integer idUnit;
	private String codeUnit;
	private String nameUnit;
	private String descriptionUnit;
	
	private String createdByUnit;
	private Date createdDateUnit;
	private String updatedByUnit;
	private Date updatedDateUnit;
	private Integer isDeleteUnit;
	
	
	@Id
	@Column(name="ID_UNIT")
	@GeneratedValue(strategy = GenerationType.TABLE, generator="M_UNIT")
	@TableGenerator(name="M_UNIT", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue ="M_UNIT_ID", valueColumnName="SEQUENCE_VALUE",
					allocationSize=1, initialValue=1)
	public Integer getIdUnit() {
		return idUnit;
	}
	public void setIdUnit(Integer idUnit) {
		this.idUnit = idUnit;
	}
	
	@Column(name="CODE_UNIT")
	public String getCodeUnit() {
		return codeUnit;
	}
	public void setCodeUnit(String codeUnit) {
		this.codeUnit = codeUnit;
	}
	
	@Column(name="NAME_UNIT")
	public String getNameUnit() {
		return nameUnit;
	}
	public void setNameUnit(String nameUnit) {
		this.nameUnit = nameUnit;
	}
	
	@Column(name="DESCRIPTION_UNIT")
	public String getDescriptionUnit() {
		return descriptionUnit;
	}
	public void setDescriptionUnit(String descriptionUnit) {
		this.descriptionUnit = descriptionUnit;
	}
	
	@Column(name="CREATED_BY_UNIT")
	public String getCreatedByUnit() {
		return createdByUnit;
	}
	public void setCreatedByUnit(String createdByUnit) {
		this.createdByUnit = createdByUnit;
	}
	
	@Column(name="CREATED_DATE_UNIT")
	public Date getCreatedDateUnit() {
		return createdDateUnit;
	}
	public void setCreatedDateUnit(Date createdDateUnit) {
		this.createdDateUnit = createdDateUnit;
	}
	
	@Column(name="UPDATE_BY_UNIT")
	public String getUpdatedByUnit() {
		return updatedByUnit;
	}
	public void setUpdatedByUnit(String updatedByUnit) {
		this.updatedByUnit = updatedByUnit;
	}
	
	@Column(name="UPDATE_DATE_UNIT")
	public Date getUpdatedDateUnit() {
		return updatedDateUnit;
	}
	public void setUpdatedDateUnit(Date updatedDateUnit) {
		this.updatedDateUnit = updatedDateUnit;
	}
	
	@Column(name="IS_DELETE_UNIT")
	public Integer getIsDeleteUnit() {
		return isDeleteUnit;
	}
	public void setIsDeleteUnit(Integer isDeleteUnit) {
		this.isDeleteUnit = isDeleteUnit;
	}
	
	
	
		}
