package com.spring.marcom149.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "M_ROLE")
public class RoleModel {
	
	public Integer idRole;
	public String codeRole;
	public String nameRole;
	public String descriptionRole;
	
	private String createdByRole;
	private Date createdDateRole;
	private String updatedByRole;
	private Date updatedDateRole;
	private Integer isDeleteRole;
	
	@Id
	@Column(name="ID_ROLE")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="M_ROLE")
	@TableGenerator(name="M_ROLE",table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
					pkColumnValue="M_ROLE_ID", valueColumnName="SEQUENCE_VALUE",
					allocationSize = 1, initialValue = 1)
	public Integer getIdRole() {
		return idRole;
	}
	public void setIdRole(Integer idRole) {
		this.idRole = idRole;
	}
	
	@Column(name="CODE_ROLE")
	public String getCodeRole() {
		return codeRole;
	}
	public void setCodeRole(String codeRole) {
		this.codeRole = codeRole;
	}
	
	@Column(name="NAME_ROLE")
	public String getNameRole() {
		return nameRole;
	}
	public void setNameRole(String nameRole) {
		this.nameRole = nameRole;
	}
	
	@Column(name="DESCRIPTION_ROLE")
	public String getDescriptionRole() {
		return descriptionRole;
	}
	public void setDescriptionRole(String descriptionRole) {
		this.descriptionRole = descriptionRole;
	}
	
	@Column(name="CREATED_BY_ROLE")
	public String getCreatedByRole() {
		return createdByRole;
	}
	public void setCreatedByRole(String createdByRole) {
		this.createdByRole = createdByRole;
	}
	
	@Column(name="CREATED_DATE_ROLE")
	public Date getCreatedDateRole() {
		return createdDateRole;
	}
	public void setCreatedDateRole(Date createdDateRole) {
		this.createdDateRole = createdDateRole;
	}
	
	@Column(name="UPDATED_BY_ROLE")
	public String getUpdatedByRole() {
		return updatedByRole;
	}
	public void setUpdatedByRole(String updatedByRole) {
		this.updatedByRole = updatedByRole;
	}
	
	@Column(name="UPDATED_DATE_ROLE")
	public Date getUpdatedDateRole() {
		return updatedDateRole;
	}
	public void setUpdatedDateRole(Date updatedDateRole) {
		this.updatedDateRole = updatedDateRole;
	}
	
	@Column(name="IS_DELETE_ROLE")
	public Integer getIsDeleteRole() {
		return isDeleteRole;
	}
	public void setIsDeleteRole(Integer isDeleteRole) {
		this.isDeleteRole = isDeleteRole;
	}
	
	
	
	
	
	

}
