package com.spring.marcom149.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_MENU")
public class MenuModel {

	private Integer idMenu;
	private String codeMenu;
	private String nameMenu;
	private String controllerName;
	private Integer isDeleteMenu;
	private String createByMenu;
	private Date createDateMenu;
	private String updateByMenu;
	private Date updateDateMenu;
	
	@Id
	@Column(name="ID_MENU")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="M_MENU")
	@TableGenerator(name="M_MENU",table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME",
	pkColumnValue="M_MENU_ID", valueColumnName="SEQUENCE_VALUE", allocationSize=1,initialValue=1)
	public Integer getIdMenu() {
		return idMenu;
	}
	public void setIdMenu(Integer idMenu) {
		this.idMenu = idMenu;
	}
	@Column(name="CODE_MENU")
	public String getCodeMenu() {
		return codeMenu;
	}
	public void setCodeMenu(String codeMenu) {
		this.codeMenu = codeMenu;
	}
	@Column(name="NAME_MENU")
	public String getNameMenu() {
		return nameMenu;
	}
	public void setNameMenu(String nameMenu) {
		this.nameMenu = nameMenu;
	}
	@Column(name="CONTROLER_NAME")
	public String getControllerName() {
		return controllerName;
	}
	public void setControllerName(String controllerName) {
		this.controllerName = controllerName;
	}
	@Column(name="IS_DELETE_MENU")
	public Integer getIsDeleteMenu() {
		return isDeleteMenu;
	}
	public void setIsDeleteMenu(Integer isDeleteMenu) {
		this.isDeleteMenu = isDeleteMenu;
	}
	@Column(name="CREATE_BY_MENU")
	public String getCreateByMenu() {
		return createByMenu;
	}
	public void setCreateByMenu(String createByMenu) {
		this.createByMenu = createByMenu;
	}
	@Column(name="CREATE_DATE_MENU")
	public Date getCreateDateMenu() {
		return createDateMenu;
	}
	public void setCreateDateMenu(Date createDateMenu) {
		this.createDateMenu = createDateMenu;
	}
	@Column(name="UPDATE_BY_MENU")
	public String getUpdateByMenu() {
		return updateByMenu;
	}
	public void setUpdateByMenu(String updateByMenu) {
		this.updateByMenu = updateByMenu;
	}
	@Column(name="UPDATE_DATE_MENU")
	public Date getUpdateDateMenu() {
		return updateDateMenu;
	}
	public void setUpdateDateMenu(Date updateDateMenu) {
		this.updateDateMenu = updateDateMenu;
	}
	
}
