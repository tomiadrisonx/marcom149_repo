package com.spring.marcom149.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.spring.marcom149.model.ProductModel;
import com.spring.marcom149.service.ProductService;
import com.spring.marcom149.tools.KodeGenerator;

@Controller
public class ProductController {
		
	@Autowired
		private ProductService productService;
	
	// url method product product
	@RequestMapping(value = "product")
	public String product() {
		return "product";
		
	}
	
	//url method halaman pop up add.jsp/tambah product
	
	@RequestMapping(value = "product/add")
	public String add(Model model) throws Exception {
		

			// Skrip utk generate KodeGenerator otomatis
			String kodeProductGenerator = "";
			Boolean cek = false;
			while (cek == false) {
				ProductModel product = new ProductModel();

				// kode digenerate dg inisial namanya, semisal KAT
				kodeProductGenerator = KodeGenerator.generator("PR");
				// setelah itu di cek dulu apa kode yang digenerate di sistem
				product = this.productService.searchByKode(kodeProductGenerator);

				// jika sudah ada maka akan di generate ulang
				if (product == null) {
					cek = true;
				}
				model.addAttribute("kodeProductGenerator", kodeProductGenerator);

			}
			// akhir sKRIP UTK GENERATE KodeGenerator otomatis

			// return kategori maksudnya memanggil file add di folder kategori
			return "product/add";
		}

	

		//url method save
		@RequestMapping(value = "product/save")
		public String save(HttpServletRequest request, Model model) throws Exception {
			/*kiri adalah variable == kanan sesuai jsp namenya*/
			
			Integer idProduct = null;
			if (request.getParameter("idProduct")!= null){
				idProduct = Integer.valueOf(request.getParameter("idProduct"));
			}
			String codeProduct = request.getParameter("codeProduct");
			String nameProduct = request.getParameter("nameProduct");
			String descriptionProduct = request.getParameter("descriptionProduct");
			
			ProductModel product = new ProductModel();
			
			
			product.setIdProduct(idProduct);
			product.setCodeProduct(codeProduct);
			product.setNameProduct(nameProduct);
			product.setDescriptionProduct(descriptionProduct);
			
			String process = request.getParameter("process");
			
			if (process.equals("create")) {
				this.productService.create(product);
				
			} else if (process.equals("update")) {
				this.productService.update(product);
				
			} else if (process.equals("delete")) {
				this.productService.delete(product);
				
			}else {
				
			}
			
			model.addAttribute("process", process);
			model.addAttribute("product", product);

			// return product maksudnya memanggil file product.jsp
			return "product";
		}
		
		//url method list
		@RequestMapping(value="product/list")
		private String list(Model model) throws Exception{
			List<ProductModel> productList= new ArrayList<ProductModel>();
			productList = this.productService.list();
			model.addAttribute("productList",productList);
			
			return "product/list";
		}
		
		//url method list
				@RequestMapping(value="product/edit")
				private String edit(Model model, HttpServletRequest request) throws Exception{
					Integer idProduct = Integer.valueOf(request.getParameter("idProduct"));
					
					ProductModel product = new ProductModel();
					product = this.productService.searchById(idProduct);
					
					model.addAttribute("product",product);
					return "product/edit";
		

	}
				
				//url method delete
				@RequestMapping(value="product/delete")
				private String delete(Model model, HttpServletRequest request) throws Exception{
					Integer idProduct = Integer.valueOf(request.getParameter("idProduct"));
					
					ProductModel product = new ProductModel();
					product = this.productService.searchById(idProduct);
					
					model.addAttribute("product",product);
					return "product/delete";
		

	}
				
				//url method delete
				@RequestMapping(value="product/detail")
				private String detail(Model model, HttpServletRequest request) throws Exception{
					Integer idProduct = Integer.valueOf(request.getParameter("idProduct"));
					
					ProductModel product = new ProductModel();
					product = this.productService.searchById(idProduct);
					
					model.addAttribute("product",product);
					return "product/detail";
		

	}
}
