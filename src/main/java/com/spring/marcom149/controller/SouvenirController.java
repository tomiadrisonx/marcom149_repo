package com.spring.marcom149.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.marcom149.model.SouvenirModel;
import com.spring.marcom149.model.UnitModel;
import com.spring.marcom149.service.SouvenirService;
import com.spring.marcom149.service.UnitService;
import com.spring.marcom149.tools.KodeGenerator;

@Controller
public class SouvenirController {

	@Autowired
	private SouvenirService souvenirService;
	
	@Autowired
	private UnitService unitService;

	// url method menu souvenir
	@RequestMapping(value = "souvenir")
	public String souvenir() {

		// return souvenir maksudnya memanggil souvenir.jsp
		return "souvenir";

	}

	// url method halaman pop up add.jsp / tambah souvenir
	@RequestMapping(value = "souvenir/add")
	public String add(Model model) throws Exception {

		// Script untuk generate kodeGenerator otomatis
		String codeSouvenirGenerator = "";
		Boolean cek = false;
		while (cek == false) {
			SouvenirModel souvenirModel = new SouvenirModel();
			// Kode digenerate dengan inisial namanya, semisal KAT
			codeSouvenirGenerator = KodeGenerator.generator("SO");

			// Setelah itu dicek dulu apakah kode yang digenerate by sistem
			// sudah ada di DB?
			souvenirModel = this.souvenirService.searchByKode(codeSouvenirGenerator);

			// Jika sudah ada, maka akan digenerate ulang
			if (souvenirModel == null) {
				cek = true;
			}

			model.addAttribute("codeSouvenirGenerator", codeSouvenirGenerator);
		}
		// Akhir Script untuk generate kodeGenerator otomatis
		
		//Skrip untuk combobox dinamis berdasrkan master
		
		List<UnitModel> unitList = null;
		unitList = this.unitService.list();
		model.addAttribute("unitList", unitList);
				
		//Akhir Skrip untuk combobox dinamis berdasrkan master

		// return souvenir maksudnya memanggil file add.jsp di folder souvenir
		return "souvenir/add";

	}

	// url method save
	@RequestMapping(value = "souvenir/save")
	public String save(HttpServletRequest request, Model model) throws Exception {

		Integer idSouvenir = null;
		if (request.getParameter("idSouvenir") != null) {
			idSouvenir = Integer.valueOf(request.getParameter("idSouvenir"));
		}
		// yang kiri variable = yang kanan dari jsp name nya
		String codeSouvenir = request.getParameter("codeSouvenir");
		String nameSouvenir = request.getParameter("nameSouvenir");
		String descriptionSouvenir = request.getParameter("descriptionSouvenir");
		
		SouvenirModel souvenirModel = new SouvenirModel();

		souvenirModel.setIdSouvenir(idSouvenir);
		souvenirModel.setCodeSouvenir(codeSouvenir);
		souvenirModel.setNameSouvenir(nameSouvenir);
		souvenirModel.setDescriptionSouvenir(descriptionSouvenir);
		
		String process = request.getParameter("process");

		if (process.equals("create")) {			
			this.souvenirService.create(souvenirModel);
		} else if (process.equals("update")) {
			this.souvenirService.update(souvenirModel);
		} else if (process.equals("delete")) {
			this.souvenirService.delete(souvenirModel);
		} else {

		}
		


		/* ini sintaks untuk kirim variabel process ke jsp */
		model.addAttribute("process", process);
		model.addAttribute("souvenirModel", souvenirModel);

		// return souvenir maksudnya memanggil file souvenir.jsp
		return "souvenir";

	}

	// url method list
	@RequestMapping(value = "souvenir/list")
	private String list(Model model) throws Exception {
		List<SouvenirModel> souvenirModelList = new ArrayList<SouvenirModel>();
		souvenirModelList = this.souvenirService.list();
		model.addAttribute("souvenirModelList", souvenirModelList);

		return "souvenir/list";
	}

	// url method edit
	@RequestMapping(value = "souvenir/edit")
	private String edit(Model model, HttpServletRequest request) throws Exception {
		Integer idSouvenir = Integer.valueOf(request.getParameter("idSouvenir"));

		SouvenirModel souvenirModel = new SouvenirModel();
		souvenirModel = this.souvenirService.searchById(idSouvenir);

		model.addAttribute("souvenirModel", souvenirModel);
		
		//Skrip untuk combobox dinamis berdasrkan master
		
				List<UnitModel> unitList = null;
				unitList = this.unitService.list();
				model.addAttribute("unitList", unitList);
						
				//Akhir Skrip untuk combobox dinamis berdasrkan master
		
		return "souvenir/edit";
	}

	// url method delete
	@RequestMapping(value = "souvenir/delete")
	private String delete(Model model, HttpServletRequest request) throws Exception {
		Integer idSouvenir = Integer.valueOf(request.getParameter("idSouvenir"));

		SouvenirModel souvenirModel = new SouvenirModel();
		souvenirModel = this.souvenirService.searchById(idSouvenir);

		model.addAttribute("souvenirModel", souvenirModel);
		
		//Skrip untuk combobox dinamis berdasrkan master
		
				List<UnitModel> unitList = null;
				unitList = this.unitService.list();
				model.addAttribute("unitList", unitList);
						
				//Akhir Skrip untuk combobox dinamis berdasrkan master
		
		return "souvenir/delete";
	}

	// url method detail
	@RequestMapping(value = "souvenir/detail")
	private String detail(Model model, HttpServletRequest request) throws Exception {
		Integer idSouvenir = Integer.valueOf(request.getParameter("idSouvenir"));

		SouvenirModel souvenirModel = new SouvenirModel();
		souvenirModel = this.souvenirService.searchById(idSouvenir);

		model.addAttribute("souvenirModel", souvenirModel);
		
		//Skrip untuk combobox dinamis berdasrkan master
		
				List<UnitModel> unitList = null;
				unitList = this.unitService.list();
				model.addAttribute("unitList", unitList);
						
				//Akhir Skrip untuk combobox dinamis berdasrkan master
		
		return "souvenir/detail";
	}
}
