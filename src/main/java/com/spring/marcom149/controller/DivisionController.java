package com.spring.marcom149.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.marcom149.model.DivisionModel;
import com.spring.marcom149.service.DivisionService;
import com.spring.marcom149.tools.KodeGenerator;

@Controller
public class DivisionController {

	@Autowired
	private DivisionService divisionService;

	// url method menu division
	@RequestMapping(value = "division")
	public String division() {

		// return division maksudnya memanggil division.jsp
		return "division";

	}

	// url method halaman pop up add.jsp / tambah division
	@RequestMapping(value = "division/add")
	public String add(Model model) throws Exception {

		// Script untuk generate kodeGenerator otomatis
		String codeDivisionGenerator = "";
		Boolean cek = false;
		while (cek == false) {
			DivisionModel divisionModel = new DivisionModel();
			// Kode digenerate dengan inisial namanya, semisal KAT
			codeDivisionGenerator = KodeGenerator.generator("DI");

			// Setelah itu dicek dulu apakah kode yang digenerate by sistem
			// sudah ada di DB?
			divisionModel = this.divisionService.searchByKode(codeDivisionGenerator);

			// Jika sudah ada, maka akan digenerate ulang
			if (divisionModel == null) {
				cek = true;
			}

			model.addAttribute("codeDivisionGenerator", codeDivisionGenerator);
		}
		// Akhir Script untuk generate kodeGenerator otomatis

		// return division maksudnya memanggil file add.jsp di folder division
		return "division/add";

	}

	// url method save
	@RequestMapping(value = "division/save")
	public String save(HttpServletRequest request, Model model) throws Exception {

		Integer idDivision = null;
		if (request.getParameter("idDivision") != null) {
			idDivision = Integer.valueOf(request.getParameter("idDivision"));
		}
		// yang kiri variable = yang kanan dari jsp name nya
		String codeDivision = request.getParameter("codeDivision");
		String nameDivision = request.getParameter("nameDivision");
		String descriptionDivision = request.getParameter("descriptionDivision");
		
		DivisionModel divisionModel = new DivisionModel();

		divisionModel.setIdDivision(idDivision);
		divisionModel.setCodeDivision(codeDivision);
		divisionModel.setNameDivision(nameDivision);
		divisionModel.setDescriptionDivision(descriptionDivision);
		
		String process = request.getParameter("process");

		if (process.equals("create")) {			
			this.divisionService.create(divisionModel);
		} else if (process.equals("update")) {
			this.divisionService.update(divisionModel);
		} else if (process.equals("delete")) {
			this.divisionService.delete(divisionModel);
		} else {

		}
		


		/* ini sintaks untuk kirim variabel process ke jsp */
		model.addAttribute("process", process);
		model.addAttribute("divisionModel", divisionModel);

		// return division maksudnya memanggil file division.jsp
		return "division";

	}

	// url method list
	@RequestMapping(value = "division/list")
	private String list(Model model) throws Exception {
		List<DivisionModel> divisionModelList = new ArrayList<DivisionModel>();
		divisionModelList = this.divisionService.list();
		model.addAttribute("divisionModelList", divisionModelList);

		return "division/list";
	}

	// url method edit
	@RequestMapping(value = "division/edit")
	private String edit(Model model, HttpServletRequest request) throws Exception {
		Integer idDivision = Integer.valueOf(request.getParameter("idDivision"));

		DivisionModel divisionModel = new DivisionModel();
		divisionModel = this.divisionService.searchById(idDivision);

		model.addAttribute("divisionModel", divisionModel);
		return "division/edit";
	}

	// url method delete
	@RequestMapping(value = "division/delete")
	private String delete(Model model, HttpServletRequest request) throws Exception {
		Integer idDivision = Integer.valueOf(request.getParameter("idDivision"));

		DivisionModel divisionModel = new DivisionModel();
		divisionModel = this.divisionService.searchById(idDivision);

		model.addAttribute("divisionModel", divisionModel);
		return "division/delete";
	}

	// url method detail
	@RequestMapping(value = "division/detail")
	private String detail(Model model, HttpServletRequest request) throws Exception {
		Integer idDivision = Integer.valueOf(request.getParameter("idDivision"));

		DivisionModel divisionModel = new DivisionModel();
		divisionModel = this.divisionService.searchById(idDivision);

		model.addAttribute("divisionModel", divisionModel);
		return "division/detail";
	}
}
