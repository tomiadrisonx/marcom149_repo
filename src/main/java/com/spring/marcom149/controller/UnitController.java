package com.spring.marcom149.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.spring.marcom149.model.UnitModel;
import com.spring.marcom149.service.UnitService;
import com.spring.marcom149.tools.KodeGenerator;

@Controller
public class UnitController {
		
	@Autowired
		private UnitService unitService;
	
	// url method unit unit
	@RequestMapping(value = "unit")
	public String unit() {
		return "unit";
		
	}
	
	//url method halaman pop up add.jsp/tambah unit
	
	@RequestMapping(value = "unit/add")
	public String add(Model model) throws Exception {
		

			// Skrip utk generate KodeGenerator otomatis
			String kodeUnitGenerator = "";
			Boolean cek = false;
			while (cek == false) {
				UnitModel unit = new UnitModel();

				// kode digenerate dg inisial namanya, semisal KAT
				kodeUnitGenerator = KodeGenerator.generator("UN");
				// setelah itu di cek dulu apa kode yang digenerate di sistem
				unit = this.unitService.searchByKode(kodeUnitGenerator);

				// jika sudah ada maka akan di generate ulang
				if (unit == null) {
					cek = true;
				}
				model.addAttribute("kodeUnitGenerator", kodeUnitGenerator);

			}
			// akhir sKRIP UTK GENERATE KodeGenerator otomatis

			// return kategori maksudnya memanggil file add di folder kategori
			return "unit/add";
		}

	

		//url method save
		@RequestMapping(value = "unit/save")
		public String save(HttpServletRequest request, Model model) throws Exception {
			/*kiri adalah variable == kanan sesuai jsp namenya*/
			
			Integer idUnit = null;
			if (request.getParameter("idUnit")!= null){
				idUnit = Integer.valueOf(request.getParameter("idUnit"));
			}
			String codeUnit = request.getParameter("codeUnit");
			String nameUnit = request.getParameter("nameUnit");
			String descriptionUnit = request.getParameter("descriptionUnit");
			
			UnitModel unit = new UnitModel();
			
			
			unit.setIdUnit(idUnit);
			unit.setCodeUnit(codeUnit);
			unit.setNameUnit(nameUnit);
			unit.setDescriptionUnit(descriptionUnit);
			
			String process = request.getParameter("process");
			
			if (process.equals("create")) {
				this.unitService.create(unit);
				
			} else if (process.equals("update")) {
				this.unitService.update(unit);
				
			} else if (process.equals("delete")) {
				this.unitService.delete(unit);
				
			}else {
				
			}
			
			model.addAttribute("process", process);
			model.addAttribute("unit", unit);

			// return unit maksudnya memanggil file unit.jsp
			return "unit";
		}
		
		//url method list
		@RequestMapping(value="unit/list")
		private String list(Model model) throws Exception{
			List<UnitModel> unitList= new ArrayList<UnitModel>();
			unitList = this.unitService.list();
			model.addAttribute("unitList",unitList);
			
			return "unit/list";
		}
		
		//url method list
				@RequestMapping(value="unit/edit")
				private String edit(Model model, HttpServletRequest request) throws Exception{
					Integer idUnit = Integer.valueOf(request.getParameter("idUnit"));
					
					UnitModel unit = new UnitModel();
					unit = this.unitService.searchById(idUnit);
					
					model.addAttribute("unit",unit);
					return "unit/edit";
		

	}
				
				//url method delete
				@RequestMapping(value="unit/delete")
				private String delete(Model model, HttpServletRequest request) throws Exception{
					Integer idUnit = Integer.valueOf(request.getParameter("idUnit"));
					
					UnitModel unit = new UnitModel();
					unit = this.unitService.searchById(idUnit);
					
					model.addAttribute("unit",unit);
					return "unit/delete";
		

	}
				
				//url method delete
				@RequestMapping(value="unit/detail")
				private String detail(Model model, HttpServletRequest request) throws Exception{
					Integer idUnit = Integer.valueOf(request.getParameter("idUnit"));
					
					UnitModel unit = new UnitModel();
					unit = this.unitService.searchById(idUnit);
					
					model.addAttribute("unit",unit);
					return "unit/detail";
		

	}
}
