package com.spring.marcom149.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.marcom149.model.CompanyModel;
import com.spring.marcom149.model.EmployeeModel;
import com.spring.marcom149.service.CompanyService;
import com.spring.marcom149.service.EmployeeService;
import com.spring.marcom149.tools.KodeGenerator;

@Controller
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private CompanyService companyService;

	// url method menu item
	@RequestMapping(value = "employee")
	public String employee() {

		// return supplier maksudnya memanggil item.jsp
		return "employee";
	}

	// url method halaman pop up add.jsp / tambah supplier
	@RequestMapping(value = "employee/add")
	public String add(Model model) throws Exception {

		// skrip untuk generate kodeGenerator
		String kodeEmployeeGenerator = "";
		Boolean cek = false;
		while (cek == false) {

			EmployeeModel employeeModel = new EmployeeModel();
			// kode digenerate dengan inisial, semisal KAT
			kodeEmployeeGenerator = KodeGenerator.generator("NIP");
			// Setelah itu dicek dulu apakah kode yang digenerate by sistem sudah ada di db?
			employeeModel = this.employeeService.searchByKode(kodeEmployeeGenerator);

			// jika sudah ad, maka akan digenerate ulang
			if (employeeModel == null) {
				cek = true;
			}

			model.addAttribute("kodeEmployeeGenerator", kodeEmployeeGenerator);

		}
		// akhir skrip untuk generate kodeGenerator

		// Skrip untuk combo box dinamis berdasarkan based on data master

		List<CompanyModel> companyModelList = null;
		companyModelList = this.companyService.list();
		model.addAttribute("companyModelList", companyModelList);

		// Akhir skrip untuk combo box dinamis berdasarkan based on data master

		// return supplier maksudnya memanggil file add.jsp di folder supplier
		return "employee/add";
	}

	// url method save
	@RequestMapping(value = "employee/save")
	public String save(HttpServletRequest request, Model model) throws Exception {
		Integer idEmployee = null;
		if (request.getParameter("idEmployee") != null) {
			idEmployee = Integer.valueOf(request.getParameter("idEmployee"));
		}

		/* yang kiri adalah variable yang kanan dari jsp name nya */
		String codeEmployee = request.getParameter("codeEmployee");
		String firstnameEmployee = request.getParameter("firstnameEmployee");
		String lastnameEmployee = request.getParameter("lastnameEmployee");
		String emailEmployee = request.getParameter("emailEmployee");
		Integer idCompany = Integer.valueOf(request.getParameter("idCompany"));

		EmployeeModel employeeModel = new EmployeeModel();

		employeeModel.setIdEmployee(idEmployee);
		employeeModel.setCodeEmployee(codeEmployee);
		employeeModel.setFirstnameEmployee(firstnameEmployee);
		employeeModel.setLastnameEmployee(lastnameEmployee);
		employeeModel.setEmailEmployee(emailEmployee);
		employeeModel.setIdCompany(idCompany);

		String process = request.getParameter("process");

		if (process.equals("create")) {
			this.employeeService.create(employeeModel);

		} else if (process.equals("update")) {
			this.employeeService.update(employeeModel);

		} else if (process.equals("delete")) {
			this.employeeService.delete(employeeModel);
		} else {

		}
		// ini syntax untuk kirim variable process ke jsp
		model.addAttribute("process", process);

		// return supplier maksudnya memanggil file supplier.jsp
		return "employee";
	}

	// url method list
	@RequestMapping(value = "employee/list")
	private String list(Model model) throws Exception {
		List<EmployeeModel> employeeModelList = new ArrayList<EmployeeModel>();
		employeeModelList = this.employeeService.list();
		model.addAttribute("employeeModelList", employeeModelList);

		return "employee/list";
	}
	
	// url method edit.jsp
		@RequestMapping(value = "employee/edit")
		private String edit(Model model, HttpServletRequest request) throws Exception {
			Integer idEmployee = Integer.valueOf(request.getParameter("idEmployee"));

			EmployeeModel employeeModel = new EmployeeModel();
			employeeModel = this.employeeService.searchById(idEmployee);

			model.addAttribute("employeeModel", employeeModel);
			
			// Skrip untuk combo box dinamis berdasarkan based on data master

			List<CompanyModel> companyModelList = null;
			companyModelList = this.companyService.list();
			model.addAttribute("companyModelList", companyModelList);

			// Akhir skrip untuk combo box dinamis berdasarkan based on data master
			
			return "employee/edit";
		}
		
		// url method edit.jsp
				@RequestMapping(value = "employee/delete")
				private String delete(Model model, HttpServletRequest request) throws Exception {
					Integer idEmployee = Integer.valueOf(request.getParameter("idEmployee"));

					EmployeeModel employeeModel = new EmployeeModel();
					employeeModel = this.employeeService.searchById(idEmployee);

					model.addAttribute("employeeModel", employeeModel);
					
					// Skrip untuk combo box dinamis berdasarkan based on data master

					List<CompanyModel> companyModelList = null;
					companyModelList = this.companyService.list();
					model.addAttribute("companyModelList", companyModelList);

					// Akhir skrip untuk combo box dinamis berdasarkan based on data master
					
					return "employee/delete";
				}
				
				// url method edit.jsp
				@RequestMapping(value = "employee/detail")
				private String detail(Model model, HttpServletRequest request) throws Exception {
					Integer idEmployee = Integer.valueOf(request.getParameter("idEmployee"));

					EmployeeModel employeeModel = new EmployeeModel();
					employeeModel = this.employeeService.searchById(idEmployee);

					model.addAttribute("employeeModel", employeeModel);
					
					// Skrip untuk combo box dinamis berdasarkan based on data master

					List<CompanyModel> companyModelList = null;
					companyModelList = this.companyService.list();
					model.addAttribute("companyModelList", companyModelList);

					// Akhir skrip untuk combo box dinamis berdasarkan based on data master
					
					return "employee/detail";
				}

}
