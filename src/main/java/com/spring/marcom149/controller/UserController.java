package com.spring.marcom149.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.marcom149.model.EmployeeModel;
import com.spring.marcom149.model.RoleModel;
import com.spring.marcom149.model.UserModel;
import com.spring.marcom149.service.EmployeeService;
import com.spring.marcom149.service.RoleService;
import com.spring.marcom149.service.UserService;

@Controller
public class UserController extends BaseController{

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "user")
	public String user() {
		return "user";
	}

	@RequestMapping(value = "user/add")
	public String add(Model model) throws Exception {

		// Skrip untuk combobox dinamis based on data master
		List<RoleModel> roleModelList = null;
		roleModelList = this.roleService.list();
		model.addAttribute("roleModelList", roleModelList);
		// Akhir Skrip untuk combobox dinamis based on data master

		// Skrip untuk combobox dinamis based on data master
		List<EmployeeModel> employeeModelList = null;
		employeeModelList = this.employeeService.list();
		model.addAttribute("employeeModelList", employeeModelList);
		// Akhir Skrip untuk combobox dinamis based on data master

		return "user/add";
	}

	@RequestMapping(value = "user/save")
	public String save(HttpServletRequest request, Model model) throws Exception {

		Integer idUser = null;
		if (request.getParameter("idUser") != null) {
			idUser = Integer.valueOf(request.getParameter("idUser"));
		}

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		Integer idRole = Integer.parseInt(request.getParameter("idRole"));
		Integer idEmployee = Integer.parseInt(request.getParameter("idEmployee"));

		UserModel userModel = new UserModel();

		userModel.setIdUser(idUser);
		userModel.setUsername(username);
		userModel.setPassword(password);
		userModel.setIdRole(idRole);
		userModel.setIdEmployee(idEmployee);

		String process = request.getParameter("process");

		if (process.equals("create")) {

			
			 UserModel userModelLogin = new UserModel(); 
			 userModelLogin = this.getUserModel();
			  
			 userModel.setCreatedByUser(userModelLogin.getUsername());
			 userModel.setCreatedDateUser(new Date()); 
			 userModel.setIsDeleteUser(0);
			 

			this.userService.create(userModel);

		} else if (process.equals("update")) {

			
			 UserModel userModelLogin = new UserModel(); 
			 userModelLogin = this.getUserModel();
			 
			 userModel.setUpdatedByUser(userModelLogin.getUsername());
			 userModel.setUpdatedDateUser(new Date()); 
			 userModel.setIsDeleteUser(0);
			 
			 UserModel umDB = new UserModel(); 
			 umDB = this.userService.searchById(idUser);
			 
			 userModel.setCreatedByUser(umDB.getCreatedByUser());
			 userModel.setCreatedDateUser(umDB.getCreatedDateUser());
			 

			this.userService.update(userModel);

		} else if (process.equals("delete")) {

			
			UserModel userModelLogin = new UserModel(); 
			userModelLogin = this.getUserModel();
			 
			userModel.setUpdatedByUser(userModelLogin.getUsername());
			userModel.setUpdatedDateUser(new Date()); 
			userModel.setIsDeleteUser(1);
			
			UserModel umDB = new UserModel(); 
			umDB = this.userService.searchById(idUser);
			
			userModel.setCreatedByUser(umDB.getCreatedByUser());
			userModel.setCreatedDateUser(umDB.getCreatedDateUser());
			 
			
			this.userService.update(userModel);
			

			/*this.userService.delete(userModel);*/

		} else {

		}
		// ini syntax utk krim var process ke jsp
		model.addAttribute("process", process);
		model.addAttribute("userModel", userModel);

		return "user";
	}

	// url method list
	@RequestMapping(value = "user/list") // comen dari ajax
	private String list(Model model) throws Exception {
		List<UserModel> userModelList = new ArrayList<UserModel>();
		userModelList = this.userService.list();
		model.addAttribute("userModelList", userModelList);
		return "user/list";

	}

	// url method edit
	@RequestMapping(value = "user/edit") // comen dari ajax
	private String edit(Model model, HttpServletRequest request) throws Exception {
		Integer idUser = Integer.valueOf(request.getParameter("idUser"));

		UserModel userModel = new UserModel();
		userModel = this.userService.searchById(idUser);

		model.addAttribute("userModel", userModel);

		// Skrip untuk combobox dinamis based on data master
		List<RoleModel> roleModelList = null;
		roleModelList = this.roleService.list();
		model.addAttribute("roleModelList", roleModelList);
		// Akhir Skrip untuk combobox dinamis based on data master

		// Skrip untuk combobox dinamis based on data master
		List<EmployeeModel> employeeModelList = null;
		employeeModelList = this.employeeService.list();
		model.addAttribute("employeeModelList", employeeModelList);
		// Akhir Skrip untuk combobox dinamis based on data master

		return "user/edit";

	}

	// url method delete
	@RequestMapping(value = "user/delete") // comen dari ajax
	private String delete(Model model, HttpServletRequest request) throws Exception {
		Integer idUser = Integer.valueOf(request.getParameter("idUser"));

		UserModel userModel = new UserModel();
		userModel = this.userService.searchById(idUser);

		model.addAttribute("userModel", userModel);

		// Skrip untuk combobox dinamis based on data master
		List<RoleModel> roleModelList = null;
		roleModelList = this.roleService.list();
		model.addAttribute("roleModelList", roleModelList);
		// Akhir Skrip untuk combobox dinamis based on data master

		// Skrip untuk combobox dinamis based on data master
		List<EmployeeModel> employeeModelList = null;
		employeeModelList = this.employeeService.list();
		model.addAttribute("employeeModelList", employeeModelList);
		// Akhir Skrip untuk combobox dinamis based on data master

		return "user/delete";

	}

}
