package com.spring.marcom149.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.marcom149.model.RoleModel;
import com.spring.marcom149.model.UserModel;
import com.spring.marcom149.service.RoleService;
import com.spring.marcom149.tools.KodeGenerator;

@Controller
public class RoleController extends BaseController{

	@Autowired
	private RoleService roleService;

	@RequestMapping(value = "role")
	public String role() {
		return "role";
	}

	@RequestMapping(value = "role/add")
	public String add(Model model) throws Exception {

		// skrip untuk generate kodeGenerator otomatis
		String codeRoleGenerator = "";
		Boolean cek = false;
		while (cek == false) {
			RoleModel roleModel = new RoleModel();
			// kode digenerate dgn inisial namanya, semisal KAT
			codeRoleGenerator = KodeGenerator.generator("RO");

			// Setelah itu dicek dulu apakah kode yg di generate by sistem sudah ada di db
			roleModel = this.roleService.searcByCode(codeRoleGenerator);

			// jika sudah ada , maka akan di generate ulang
			if (roleModel == null) {
				cek = true;
			}

			model.addAttribute("codeRoleGenerator", codeRoleGenerator);

		}
		// akhir skrip untuk generate kodeGenerator otomatis

		return "role/add";
	}

	@RequestMapping(value = "role/save")
	public String save(HttpServletRequest request, Model model) throws Exception {

		Integer idRole = null;
		if (request.getParameter("idRole") != null) {
			idRole = Integer.valueOf(request.getParameter("idRole"));
		}

		String codeRole = request.getParameter("codeRole");
		String nameRole = request.getParameter("nameRole");
		String descriptionRole = request.getParameter("descriptionRole");

		RoleModel roleModel = new RoleModel();

		roleModel.setIdRole(idRole);
		roleModel.setCodeRole(codeRole);
		roleModel.setNameRole(nameRole);
		roleModel.setDescriptionRole(descriptionRole);

		String process = request.getParameter("process");

		if (process.equals("create")) {
			
			UserModel userModel = new UserModel();
			userModel = this.getUserModel();
			
			roleModel.setCreatedByRole(userModel.getUsername());
			roleModel.setCreatedDateRole(new Date());
			roleModel.setIsDeleteRole(0);

			this.roleService.create(roleModel);

		} else if (process.equals("update")) {
			
			UserModel userModel = new UserModel();
			userModel = this.getUserModel();
			
			roleModel.setUpdatedByRole(userModel.getUsername());
			roleModel.setUpdatedDateRole(new Date());
			roleModel.setIsDeleteRole(0);
			
			RoleModel rmDB = new RoleModel();
			rmDB = this.roleService.searchById(idRole);
			
			roleModel.setCreatedByRole(rmDB.getCreatedByRole());
			roleModel.setCreatedDateRole(rmDB.getCreatedDateRole());

			this.roleService.update(roleModel);

		} else if (process.equals("delete")) {
			
			UserModel userModel = new UserModel();
			userModel = this.getUserModel();
			
			roleModel.setUpdatedByRole(userModel.getUsername());
			roleModel.setUpdatedDateRole(new Date());
			roleModel.setIsDeleteRole(1);
			
			RoleModel rmDB = new RoleModel();
			rmDB = this.roleService.searchById(idRole);
			
			roleModel.setCreatedByRole(rmDB.getCreatedByRole());
			roleModel.setCreatedDateRole(rmDB.getCreatedDateRole());
			
			this.roleService.update(roleModel);

			/*this.roleService.delete(roleModel);*/

		} else {

		}
		// ini syntax utk krim var process ke jsp
		model.addAttribute("process", process);
		model.addAttribute("roleModel", roleModel);

		return "role";
	}
	
	// url method list
		@RequestMapping(value = "role /list") // comen dari ajax
		private String list(Model model) throws Exception {
			List<RoleModel> roleModelList = new ArrayList<RoleModel>();
			roleModelList = this.roleService.list();
			model.addAttribute("roleModelList", roleModelList);
			return "role/list";

		}

		// url method edit
		@RequestMapping(value = "role/edit") // comen dari ajax
		private String edit(Model model, HttpServletRequest request) throws Exception {
			Integer idRole = Integer.valueOf(request.getParameter("idRole"));

			RoleModel roleModel = new RoleModel();
			roleModel = this.roleService.searchById(idRole);

			model.addAttribute("roleModel", roleModel);

			return "role/edit";

		}

		// url method delete
		@RequestMapping(value = "role/delete") // comen dari ajax
		private String delete(Model model, HttpServletRequest request) throws Exception {
			Integer idRole = Integer.valueOf(request.getParameter("idRole"));

			RoleModel roleModel = new RoleModel();
			roleModel = this.roleService.searchById(idRole);

			model.addAttribute("roleModel", roleModel);

			return "role/delete";

		}

		

}
