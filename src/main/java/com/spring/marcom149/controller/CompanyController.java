package com.spring.marcom149.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.marcom149.model.CompanyModel;
import com.spring.marcom149.service.CompanyService;
import com.spring.marcom149.tools.KodeGenerator;


@Controller
public class CompanyController {

	@Autowired
	private CompanyService companyService;

	// url method menu item
	@RequestMapping(value = "company")
	public String company() {

		// return supplier maksudnya memanggil item.jsp
		return "company";
	}

	// url method halaman pop up add.jsp / tambah supplier
	@RequestMapping(value = "company/add")
	public String add(Model model) throws Exception {

		// skrip untuk generate kodeGenerator
		String kodeCompanyGenerator = "";
		Boolean cek = false;
		while (cek == false) {

			CompanyModel companyModel = new CompanyModel();
			// kode digenerate dengan inisial, semisal KAT
			kodeCompanyGenerator = KodeGenerator.generator("CP");
			// Setelah itu dicek dulu apakah kode yang digenerate by sistem sudah ada di db?
			companyModel = this.companyService.searchByKode(kodeCompanyGenerator);

			// jika sudah ad, maka akan digenerate ulang
			if (companyModel == null) {
				cek = true;
			}

			model.addAttribute("kodeCompanyGenerator", kodeCompanyGenerator);

		}
		// akhir skrip untuk generate kodeGenerator

		// return supplier maksudnya memanggil file add.jsp di folder supplier
		return "company/add";
	}

	// url method save
	@RequestMapping(value = "company/save")
	public String save(HttpServletRequest request, Model model) throws Exception {
		Integer idCompany = null;
		if (request.getParameter("idCompany") != null) {
			idCompany = Integer.valueOf(request.getParameter("idCompany"));
		}

		/* yang kiri adalah variable yang kanan dari jsp name nya */
		String codeCompany = request.getParameter("codeCompany");
		String nameCompany = request.getParameter("nameCompany");
		String addressCompany = request.getParameter("addressCompany");
		String phoneCompany = request.getParameter("phoneCompany");
		String emailCompany = request.getParameter("emailCompany");

		CompanyModel companyModel = new CompanyModel();

		companyModel.setIdCompany(idCompany);
		companyModel.setCodeCompany(codeCompany);
		companyModel.setNameCompany(nameCompany);
		companyModel.setAddressCompany(addressCompany);
		companyModel.setPhoneCompany(phoneCompany);
		companyModel.setEmailCompany(emailCompany);

		String process = request.getParameter("process");

		if (process.equals("create")) {
			this.companyService.create(companyModel);

		} else if (process.equals("update")) {
			this.companyService.update(companyModel);

		} else if (process.equals("delete")) {
			this.companyService.delete(companyModel);
		} else {

		}
		// ini syntax untuk kirim variable process ke jsp
		model.addAttribute("process", process);
		model.addAttribute("companyModel", companyModel);

		// return supplier maksudnya memanggil file supplier.jsp
		return "company";
	}

	// url method list
	@RequestMapping(value = "company/list")
	private String list(Model model) throws Exception {
		List<CompanyModel> companyModelList = new ArrayList<CompanyModel>();
		companyModelList = this.companyService.list();
		model.addAttribute("companyModelList", companyModelList);

		return "company/list";
	}

	// url method edit.jsp
	@RequestMapping(value = "company/edit")
	private String edit(Model model, HttpServletRequest request) throws Exception {
		Integer idCompany = Integer.valueOf(request.getParameter("idCompany"));

		CompanyModel companyModel = new CompanyModel();
		companyModel = this.companyService.searchById(idCompany);

		model.addAttribute("companyModel", companyModel);
		return "company/edit";
	}
	
	// url method delete.jsp
		@RequestMapping(value = "company/delete")
		private String delete(Model model, HttpServletRequest request) throws Exception {
			Integer idCompany = Integer.valueOf(request.getParameter("idCompany"));

			CompanyModel companyModel = new CompanyModel();
			companyModel = this.companyService.searchById(idCompany);

			model.addAttribute("companyModel", companyModel);
			return "company/delete";
		}
		
		// url method delete.jsp
				@RequestMapping(value = "company/detail")
				private String detail(Model model, HttpServletRequest request) throws Exception {
					Integer idCompany = Integer.valueOf(request.getParameter("idCompany"));

					CompanyModel companyModel = new CompanyModel();
					companyModel = this.companyService.searchById(idCompany);

					model.addAttribute("companyModel", companyModel);
					return "company/detail";
				}

}
