package com.spring.marcom149.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.UserDao;
import com.spring.marcom149.model.UserModel;

@Repository
public class UserDaoImpl implements UserDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(UserModel userModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(userModel);
		
	}

	@Override
	public void update(UserModel userModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(userModel);
		
	}

	@Override
	public void delete(UserModel userModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(userModel);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserModel> list() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<UserModel> result = session.createQuery("from UserModel where isDeleteUser = 0").list();
		return result;
	}

	@Override
	public UserModel searchById(Integer idUser) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		UserModel result = session.get(UserModel.class, idUser);
		return result;
	}

	@Override
	public UserModel searchByUsernamePassword(String username, String password) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		UserModel result = null;
		try {
			result = (UserModel) session.createQuery(" from UserModel "
													+ " where username = '"+username+"' "
													+ " and password = '"+password+"' ").getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println();
		}
		return result;
	}

}
