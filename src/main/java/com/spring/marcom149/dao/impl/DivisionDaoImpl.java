package com.spring.marcom149.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.DivisionDao;
import com.spring.marcom149.model.DivisionModel;

@Repository
public class DivisionDaoImpl implements DivisionDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(DivisionModel divisionModel) throws Exception {
		// TODO Auto-generated method stub

		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();

		// seesion.save adalah query insert into dengan hibernate
		session.save(divisionModel);
	}

	@Override
	public void update(DivisionModel divisionModel) throws Exception {
		// TODO Auto-generated method stub
		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();

		// seesion.save adalah query update data dengan hibernate
		session.update(divisionModel);
	}

	@Override
	public void delete(DivisionModel divisionModel) throws Exception {
		// TODO Auto-generated method stub
		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();

		// seesion.save adalah query update data dengan hibernate
		session.delete(divisionModel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DivisionModel> list() throws Exception {
		// TODO Auto-generated method stub
		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();
		List<DivisionModel> result = session.createQuery(" from DivisionModel ").list();

		return result;
	}

	@Override
	public DivisionModel searchByKode(String codeDivision) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		DivisionModel result = null;
		try {
			result = (DivisionModel) session
					.createQuery("from DivisionModel Where codeDivision = '" + codeDivision + "' ").getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public DivisionModel searchById(Integer idDivision) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		DivisionModel result = session.get(DivisionModel.class, idDivision);
		return result;
	}

}
