package com.spring.marcom149.dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.ProductDao;
import com.spring.marcom149.model.ProductModel;

@Repository
public class ProductDaoImpl implements ProductDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(ProductModel product) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		// session save = query insert into dgn hibernate
		session.save(product);

	}

	@Override
	public void update(ProductModel product) throws Exception {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();

		// session save = query insert into dgn hibernate
		session.update(product);

	}

	@Override
	public void delete(ProductModel product) throws Exception {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();

		// session save = query insert into dgn hibernate
		session.delete(product);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductModel> list() throws Exception {
		// TODO Auto-generated method stub
		// syntax awal untuk query hibernate

		Session session = this.sessionFactory.getCurrentSession();
		List<ProductModel> result = session.createQuery("from ProductModel").list();

		return result;
	}

	@Override
	public ProductModel searchByKode(String codeProduct) throws Exception {
		// TODO Auto-generated method stu
		Session session = this.sessionFactory.getCurrentSession();
		ProductModel result = null;
		try {
			result = (ProductModel) session.createQuery("from ProductModel Where codeProduct ='" + codeProduct + "' ")
					.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public ProductModel searchById(Integer idProduct) throws Exception {
		// TODO Auto-generated method stu
		Session session = this.sessionFactory.getCurrentSession();
		ProductModel result = session.get(ProductModel.class, idProduct);
		return result;
	}
}
