package com.spring.marcom149.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.MenuAccessDao;
import com.spring.marcom149.model.MenuAccessModel;


@Repository
public class MenuAccessDaoImpl implements MenuAccessDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(MenuAccessModel menuAccessModel) throws Exception {
		// TODO Auto-generated method stub
		//ini sintax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();
		//query insert into dgn hibernate
		session.save(menuAccessModel);
	}

	@Override
	public void update(MenuAccessModel menuAccessModel) throws Exception {
		// TODO Auto-generated method stub
		//ini sintax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();
		//query insert into dgn hibernate
		session.update(menuAccessModel);
	}

	@Override
	public void delete(MenuAccessModel menuAccessModel) throws Exception {
		// TODO Auto-generated method stub
		//ini sintax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();
		//query insert into dgn hibernate
		session.delete(menuAccessModel);
	}

	@Override
	public List<MenuAccessModel> list() throws Exception {
		// TODO Auto-generated method stub
		//ini sintax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<MenuAccessModel> result = session.createQuery("From MenuAccessModel where isDeleteMenuAccess = 0").list();
			
		return result;
	}

	@Override
	public MenuAccessModel searchById(Integer idMenuAccess) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		MenuAccessModel result = session.get(MenuAccessModel.class, idMenuAccess);
		return result;
	}

}
