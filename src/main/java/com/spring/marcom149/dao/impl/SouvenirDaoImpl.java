package com.spring.marcom149.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.SouvenirDao;
import com.spring.marcom149.model.SouvenirModel;

@Repository
public class SouvenirDaoImpl implements SouvenirDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(SouvenirModel souvenirModel) throws Exception {
		// TODO Auto-generated method stub

		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();

		// seesion.save adalah query insert into dengan hibernate
		session.save(souvenirModel);
	}

	@Override
	public void update(SouvenirModel souvenirModel) throws Exception {
		// TODO Auto-generated method stub
		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();

		// seesion.save adalah query update data dengan hibernate
		session.update(souvenirModel);
	}

	@Override
	public void delete(SouvenirModel souvenirModel) throws Exception {
		// TODO Auto-generated method stub
		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();

		// seesion.save adalah query update data dengan hibernate
		session.delete(souvenirModel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SouvenirModel> list() throws Exception {
		// TODO Auto-generated method stub
		// ini sintaks awal untuk query hibernate
		Session session = this.sessionFactory.getCurrentSession();
		List<SouvenirModel> result = session.createQuery(" from SouvenirModel ").list();

		return result;
	}

	@Override
	public SouvenirModel searchByKode(String codeSouvenir) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		SouvenirModel result = null;
		try {
			result = (SouvenirModel) session
					.createQuery("from SouvenirModel Where codeSouvenir = '" + codeSouvenir + "' ").getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public SouvenirModel searchById(Integer idSouvenir) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		SouvenirModel result = session.get(SouvenirModel.class, idSouvenir);
		return result;
	}

}
