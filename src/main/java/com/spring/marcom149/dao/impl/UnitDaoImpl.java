package com.spring.marcom149.dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.UnitDao;
import com.spring.marcom149.model.UnitModel;

@Repository
public class UnitDaoImpl implements UnitDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(UnitModel unit) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		// session save = query insert into dgn hibernate
		session.save(unit);

	}

	@Override
	public void update(UnitModel unit) throws Exception {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();

		// session save = query insert into dgn hibernate
		session.update(unit);

	}

	@Override
	public void delete(UnitModel unit) throws Exception {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();

		// session save = query insert into dgn hibernate
		session.delete(unit);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UnitModel> list() throws Exception {
		// TODO Auto-generated method stub
		// syntax awal untuk query hibernate

		Session session = this.sessionFactory.getCurrentSession();
		List<UnitModel> result = session.createQuery("from UnitModel").list();

		return result;
	}

	@Override
	public UnitModel searchByKode(String codeUnit) throws Exception {
		// TODO Auto-generated method stu
		Session session = this.sessionFactory.getCurrentSession();
		UnitModel result = null;
		try {
			result = (UnitModel) session.createQuery("from UnitModel Where codeUnit ='" + codeUnit + "' ")
					.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public UnitModel searchById(Integer idUnit) throws Exception {
		// TODO Auto-generated method stu
		Session session = this.sessionFactory.getCurrentSession();
		UnitModel result = session.get(UnitModel.class, idUnit);
		return result;
	}
}
