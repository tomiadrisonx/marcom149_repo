package com.spring.marcom149.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.CompanyDao;
import com.spring.marcom149.model.CompanyModel;


@Repository
public class CompanyDaoImpl implements CompanyDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(CompanyModel companyModel) throws Exception {
		// TODO Auto-generated method stub
		//adalah syntax awal untuk query hibernatenya
				Session session = this.sessionFactory.getCurrentSession()	;
				
				//session.save adalah query into dengan hibernate
				session.save(companyModel);
				
	}

	@Override
	public void update(CompanyModel companyModel) throws Exception {
		// TODO Auto-generated method stub
		//adalah syntax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession()	;
		
		//session.save adalah query into dengan hibernate
		session.update(companyModel);
	}

	@Override
	public void delete(CompanyModel companyModel) throws Exception {
		// TODO Auto-generated method stub
		//adalah syntax awal untuk query hibernatenya
		Session session = this.sessionFactory.getCurrentSession()	;
		
		//session.save adalah query into dengan hibernate
		session.delete(companyModel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyModel> list() throws Exception {
		// TODO Auto-generated method stub
		//adalah syntax awal untuk query hibernatenya
				Session session = this.sessionFactory.getCurrentSession()	;
				
				List<CompanyModel> result = session.createQuery(" from CompanyModel ").list();

		return result;
	}

	@Override
	public CompanyModel searchByKode(String codeCompany) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		CompanyModel result = null;
		try {
			result = (CompanyModel) session.createQuery("from CompanyModel Where codeCompany = '"+codeCompany+"' ").getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public CompanyModel searchById(Integer idCompany) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		CompanyModel result = session.get(CompanyModel.class, idCompany);
		return result;
	}

}
