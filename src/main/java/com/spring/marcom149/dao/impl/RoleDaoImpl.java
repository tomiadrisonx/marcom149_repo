package com.spring.marcom149.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.marcom149.dao.RoleDao;
import com.spring.marcom149.model.RoleModel;

@Repository
public class RoleDaoImpl implements RoleDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(RoleModel roleModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(roleModel);
		
	}

	@Override
	public void update(RoleModel roleModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(roleModel);
		
	}

	@Override
	public void delete(RoleModel roleModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(roleModel);
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RoleModel> list() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<RoleModel> result = session.createQuery("from RoleModel where isDeleteRole = 0").list();
		return result;
	}

	@Override
	public RoleModel searcByCode(String codeRole) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		RoleModel result = null;
		try {
			result = (RoleModel) session.createQuery("from RoleModel where codeRole = '"+codeRole+"' ").getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public RoleModel searchById(Integer idRole) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		RoleModel result = session.get(RoleModel.class, idRole);
		return result;
	}

}
