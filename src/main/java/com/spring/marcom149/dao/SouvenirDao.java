package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.SouvenirModel;

public interface SouvenirDao {
	
	/*transaksi create*/
	public void create(SouvenirModel souvenirModel) throws Exception;
	
	/*transaksi update*/
	public void update(SouvenirModel souvenirModel) throws Exception;
	
	/*transaksi delete*/
	public void delete(SouvenirModel souvenirModel) throws Exception;
	
	/*transaksi search*/
	public List<SouvenirModel> list() throws Exception;
	
	/*transaksi searchByKode*/
	public SouvenirModel searchByKode(String codeSouvenir) throws Exception;
	
	/*transaksi searchById*/
	public SouvenirModel searchById(Integer idSouvenir) throws Exception;

}
