package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.RoleModel;

public interface RoleDao {
	
	public void create(RoleModel roleModel) throws Exception;
	
	public void update(RoleModel roleModel) throws Exception;
	
	public void delete(RoleModel roleModel) throws Exception;
	
	public List<RoleModel> list() throws Exception;
	
	public RoleModel searcByCode(String codeRole) throws Exception;
	
	public RoleModel searchById(Integer idRole) throws Exception;

}
