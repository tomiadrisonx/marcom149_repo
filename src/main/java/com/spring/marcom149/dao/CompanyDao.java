package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.CompanyModel;


public interface CompanyDao {
	
public void create (CompanyModel companyModel) throws Exception;
	
	public void update (CompanyModel companyModel) throws Exception;
	
	public void delete (CompanyModel companyModel) throws Exception;
	
	public List<CompanyModel>list() throws Exception;
	
	/*query searchByKode*/
	public CompanyModel searchByKode(String codeCompany) throws Exception;
	
	/*query searchById*/
	public CompanyModel searchById(Integer idCompany) throws Exception;


}
