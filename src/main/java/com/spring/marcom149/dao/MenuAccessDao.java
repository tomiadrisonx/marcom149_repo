package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.MenuAccessModel;

public interface MenuAccessDao {

	public void create(MenuAccessModel menuAccessModel) throws Exception;
	public void update(MenuAccessModel menuAccessModel) throws Exception;
	public void delete(MenuAccessModel menuAccessModel) throws Exception;
	public  List<MenuAccessModel> list() throws Exception;
	public MenuAccessModel searchById(Integer idMenuAccess) throws Exception;
	
}
