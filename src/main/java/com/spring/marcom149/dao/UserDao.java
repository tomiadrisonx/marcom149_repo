package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.UserModel;

public interface UserDao {
	
	public void create(UserModel userModel) throws Exception;
	
	public void update(UserModel userModel) throws Exception;
	
	public void delete(UserModel userModel) throws Exception;
	
	public List<UserModel> list() throws Exception;
	
	/* transaksi searchById */
	public UserModel searchById(Integer idUser) throws Exception;
	
	public UserModel searchByUsernamePassword(String username, String password) throws Exception;
	
	
	

}
