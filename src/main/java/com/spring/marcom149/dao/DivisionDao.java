package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.DivisionModel;

public interface DivisionDao {
	
	/*transaksi create*/
	public void create(DivisionModel divisionModel) throws Exception;
	
	/*transaksi update*/
	public void update(DivisionModel divisionModel) throws Exception;
	
	/*transaksi delete*/
	public void delete(DivisionModel divisionModel) throws Exception;
	
	/*transaksi search*/
	public List<DivisionModel> list() throws Exception;
	
	/*transaksi searchByKode*/
	public DivisionModel searchByKode(String codeDivision) throws Exception;
	
	/*transaksi searchById*/
	public DivisionModel searchById(Integer idDivision) throws Exception;

}
