package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.EmployeeModel;

public interface EmployeeDao {

public void create (EmployeeModel employeeModel) throws Exception;
	
	public void update (EmployeeModel employeeModel) throws Exception;
	
	public void delete (EmployeeModel employeeModel) throws Exception;
	
	public List<EmployeeModel>list() throws Exception;
	
	/*query searchByKode*/
	public EmployeeModel searchByKode(String codeEmployee) throws Exception;
	
	/*query searchById*/
	public EmployeeModel searchById(Integer idEmployee) throws Exception;


}
