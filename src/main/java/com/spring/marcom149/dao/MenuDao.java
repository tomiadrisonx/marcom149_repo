package com.spring.marcom149.dao;

import java.util.List;

import com.spring.marcom149.model.MenuModel;

public interface MenuDao {

	public void create(MenuModel menuModel) throws Exception;

	public void update(MenuModel menuModel) throws Exception;

	public void delete(MenuModel menuModel) throws Exception;

	public List<MenuModel> list() throws Exception;

	public MenuModel searchByCode(String codeMenu) throws Exception;

	public MenuModel searchById(Integer idMenu) throws Exception;

	// Dao untuk tampilan hirarki menu
	public List<MenuModel> getAllMenuTree() throws Exception;

	public List<MenuModel> getAllMenuTreeByRole(Integer idRole) throws Exception;
}
